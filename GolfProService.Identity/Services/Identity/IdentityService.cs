﻿using GolfProService.Identity.Models.DTO.Signin;
using GolfProService.Identity.Models.DTO.Signup;
using GolfProService.Identity.Services.Utility;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GolfProService.Identity.Services.Identity
{
    public class IdentityService : IIdentityService
    {
        private IServiceUtility ServiceUtility = null;
        public IdentityService(IServiceUtility serviceUtility)
        {
            ServiceUtility = serviceUtility;
        }

       /// <summary>
       /// Make call to marketplace api service to get User info
       /// </summary>
       /// <param name="userToken"></param>
       /// <returns></returns>
        public async Task<UserDTO> GetUserInfoByUserToken(string userToken)
        {
            UserDTO userInfo = null;
            try
            {
                userInfo = await ServiceUtility.GetDataFromService<UserDTO>("/api/v4/users?details=n", "MarketplaceStagingAPIs", userToken);
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return userInfo;
        }

        /// <summary>
        /// Make call to marketplace Signin API service call and receive the valid user token
        /// </summary>
        /// <param name="signinRequest"></param>
        /// <returns></returns>
        public async Task<string> GetValidUserToken(SigninRequestDTO signinRequest)
        {
            SigninResponseDTO userAuthInfo = null;
            try
            {
                userAuthInfo = await ServiceUtility.PostDataToService<SigninResponseDTO>("/api/v4/users/sign-in", "MarketplaceStagingAPIs","", signinRequest);                
            }
            catch(Exception ex)
            {
                throw ex;
            }
            return userAuthInfo.token;
        }

        /// <summary>
        /// Marketplace API service call for Professional Signup 
        /// </summary>
        /// <param name="proInfo"></param>
        /// <returns></returns>
        public async Task<SignupResponseDTO> SignupAtMarketplace(UserDetails signUpDetails)
        {
            SignupResponseDTO response = null;
            try
            {
                response = await ServiceUtility.PostDataToService<SignupResponseDTO>("/api/v4/users", "MarketplaceStagingAPIs", "", CreateSignupRequestData(signUpDetails));
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return response;
        }

        /// <summary>
        ///  Make call to marketplace api service to get User info
        /// </summary>
        /// <param name="authToken"></param>
        /// <returns></returns>
        public async Task<UserDTO> GetUserFromMarketplace(string authToken)
        {
            UserDTO response = null;
            try
            {
                response = await ServiceUtility.GetDataFromService<UserDTO>("/api/v4/users", "MarketplaceStagingAPIs", authToken);
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return response;
        }

        /// <summary>
        /// Create professional info object for Marketplace API call
        /// </summary>
        /// <param name="proInfo"></param>
        /// <returns></returns>
        private SignupRequestDTO CreateSignupRequestData(UserDetails details)
        {
            SignupRequestDTO request = null;
            try
            {
                request = new SignupRequestDTO();
                request.email = details.email;
                request.password = details.password;
                request.first_name = details.firstname;
                request.last_name = details.lastname;
                request.address_zipcode = details.zip;
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return request;
        }
    }
}
