﻿using GolfProService.Identity.Models.DTO.Signin;
using GolfProService.Identity.Models.DTO.Signup;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GolfProService.Identity.Services.Identity
{
    public interface IIdentityService
    {
        Task<string> GetValidUserToken(SigninRequestDTO signinRequest);
        Task<SignupResponseDTO> SignupAtMarketplace(UserDetails signUpDetails);
        Task<UserDTO> GetUserFromMarketplace(string authToken);
        Task<UserDTO> GetUserInfoByUserToken(string userToken);
    }
}
