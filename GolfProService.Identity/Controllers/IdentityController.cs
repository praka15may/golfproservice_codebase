﻿using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using GolfProService.Identity.Models.DTO.Signin;
using GolfProService.Identity.Models.DTO.Signup;
using GolfProService.Identity.Repository.Identity;
using GolfProService.Identity.Services.Identity;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;

namespace GolfProService.Identity.Controllers
{
    [Route("api/Identity/[Action]")]
    public class IdentityController : Controller
    {
        private IIdentityService IdentityService = null;
        private IIdentityRepository IdentityRepository = null;
        public IdentityController(IIdentityService identityService, IIdentityRepository identityRepository)
        {
            IdentityService = identityService;
            IdentityRepository = identityRepository;
        }
        
        /// <summary>
        /// Authenticate the user and returns user auth token along with user type mapping(Pro/Player)
        /// </summary>
        /// <param name="signinRequestDTO"></param>
        /// <returns></returns>
        [HttpPost]
        [ProducesResponseType(200, Type = typeof(SystemUserInfoDTO))]
        public async Task<ActionResult> Signin([FromBody]SigninRequestDTO signinRequestDTO)
        {
            dynamic obj = new ExpandoObject();
            try
            {
                // Model validations
                if (ModelState.IsValid)
                {
                    var userToken = await IdentityService.GetValidUserToken(signinRequestDTO);
                    obj.Result = await IdentityRepository.SaveUserInfo(userToken);
                }
                else
                {
                    // Returns validation errors by comma seprated
                    var msg = String.Join(", ", ModelState.Keys.SelectMany(key => this.ModelState[key].Errors.Select(x => key + ": " + x.ErrorMessage)));
                    return BadRequest(msg);
                }
                
            }
            catch(Exception ex)
            {
                return BadRequest(JsonConvert.DeserializeObject<object>(ex.Message));
            }

            return Ok(obj.Result);
        }

        /// <summary>
        /// Map the user with user type(Pro/Player)
        /// </summary>
        /// <param name="userRoleDTO"></param>
        /// <returns></returns>
        [HttpPut]
        [ProducesResponseType(200, Type = typeof(UserRoleDTO))]
        public async Task<ActionResult> Signin([FromBody]UserRoleDTO userRoleDTO)
        {
            dynamic obj = new ExpandoObject();
            try
            {
                if (ModelState.IsValid)
                {
                    obj.Result = await IdentityRepository.UpdateUserInfoByID(userRoleDTO);
                }
                else
                {
                    // Returns validation errors by comma seprated
                    var msg = String.Join(", ", ModelState.Keys.SelectMany(key => this.ModelState[key].Errors.Select(x => key + ": " + x.ErrorMessage)));
                    return BadRequest(msg);
                }
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }

            return Ok(obj.Result);
        }


        /// <summary>
        /// Sign Up method for Professional/Player to register into GolfPro Web/Mobile application 
        /// </summary>
        /// <param name="signUpDetails"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<IActionResult> SignUp([FromBody] UserDetails signUpDetails)
        {
            try
            {
                var result = await IdentityService.SignupAtMarketplace(signUpDetails);
                var userAdded = await IdentityRepository.SaveUser(result.Token,signUpDetails.role);
                if(userAdded!=null)
                return Ok(new { token = result.Token, confirmation_url = result.Confirmation_URL,role = userAdded.UserTypeID,userid = userAdded.GolfProUserID });
                 else
                return StatusCode(StatusCodes.Status500InternalServerError, new { msg = "Please try After Some Time" });
            }
            catch (Exception ex)
            {
                object msg = JsonConvert.DeserializeObject(ex.Message);
                return BadRequest(msg);
            }
        }
    }
}