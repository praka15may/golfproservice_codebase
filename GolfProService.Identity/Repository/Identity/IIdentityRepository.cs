﻿using GolfProService.Identity.Models;
using GolfProService.Identity.Models.DTO.Signin;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GolfProService.Identity.Repository.Identity
{
    public interface IIdentityRepository
    {
        Task<SystemUserInfoDTO> SaveUserInfo(string userToken);
        Task<UserRoleDTO> UpdateUserInfoByID(UserRoleDTO userRoleDTO);
       // Task<UserInfoDTO> GetUserInfoByToken(string userToken);
        Task<User> SaveUser(string userToken,int roleID);
    }
}
