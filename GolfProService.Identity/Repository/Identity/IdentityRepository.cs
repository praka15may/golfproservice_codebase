﻿using GolfProService.Identity.Models;
using GolfProService.Identity.Models.DTO.Signin;
using GolfProService.Identity.Services.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GolfProService.Identity.Repository.Identity
{
    public class IdentityRepository : IIdentityRepository
    {
        IIdentityService IdentityService = null;
        IdentityContext IdentityContext = null;
        public IdentityRepository(IIdentityService identityService, IdentityContext identityContext)
        {
            IdentityService = identityService;
            IdentityContext = identityContext;
        }

        /// <summary>
        /// Save the user info into identity service database
        /// </summary>
        /// <param name="userToken"></param>
        /// <returns></returns>
        public async Task<SystemUserInfoDTO> SaveUserInfo(string userToken)
        {
            SystemUserInfoDTO systemUserInfo = null;
            UserDTO userInfo = null;
            User user = null;
            try
            {
                user = GetIdentityUserInfoByToken(userToken);
                if(user == null)
                {
                    userInfo = await IdentityService.GetUserInfoByUserToken(userToken);
                    user = MapUserInfoToDataModel(userToken, userInfo);
                    IdentityContext.Add(user);
                    await IdentityContext.SaveChangesAsync();                    
                }
                systemUserInfo = CreateUserInfoObject(user);

            }
            catch (Exception ex)
            {
                throw ex;
            }

            return systemUserInfo;
        }

        /// <summary>
        /// Create logged in user record in IdentityDB 
        /// </summary>
        /// <param name="authToken"></param>
        /// <param name="roleID"></param>
        /// <returns></returns>
        public async Task<User> SaveUser(string authToken, int roleID)
        {
            User user = null;
            try
            {
                var marketPlaceUser = await IdentityService.GetUserFromMarketplace(authToken);
                if (marketPlaceUser != null)
                {
                    User identityUser = new User() { AuthToken = authToken, GolfProUserID = marketPlaceUser.user.id, DateRegistered = marketPlaceUser.user.created_at, UserTypeID = roleID };
                    IdentityContext.Add(identityUser);
                    int status = await IdentityContext.SaveChangesAsync();
                    if (status == 1)
                        user = identityUser;
                    
                }
                return user;
            }
            catch(Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Update the user role
        /// </summary>
        /// <param name="userRoleDTO"></param>
        /// <returns></returns>
        public async Task<UserRoleDTO> UpdateUserInfoByID(UserRoleDTO userRoleDTO)
        {
            User user = null;
            try
            {
                user = IdentityContext.Users.FirstOrDefault(s => s.GolfProUserID == userRoleDTO.userid);
                if(user != null)
                {
                    user.UserTypeID = userRoleDTO.role;
                    IdentityContext.Update(user);
                    await IdentityContext.SaveChangesAsync();
                }
                else
                {
                    throw new Exception("User does not exist");
                }
            }
            catch(Exception ex)
            {
                throw ex;
            }

            return userRoleDTO;
        }

        #region Private Methods

        /// <summary>
        /// Object mapping for the sign in service response
        /// </summary>
        /// <param name="user"></param>
        /// <returns></returns>
        private SystemUserInfoDTO CreateUserInfoObject(User user)
        {
            SystemUserInfoDTO userInfo = null;
            try
            {
                userInfo = new SystemUserInfoDTO
                {
                    tagged = false,
                    token = user.AuthToken,
                    role = user.UserTypeID,
                    userid = user.GolfProUserID
                };

                if (userInfo.role != null)
                    userInfo.tagged = true;
            }
            catch(Exception ex)
            {
                throw ex;
            }

            return userInfo;
        }

        /// <summary>
        /// Object mapping of DTO to data model
        /// </summary>
        /// <param name="userToken"></param>
        /// <param name="userInfo"></param>
        /// <returns></returns>
        private User MapUserInfoToDataModel(string userToken, UserDTO userInfo)
        {
            User user = null;
            try
            {
                user = new User
                {
                    AuthToken = userToken,
                    GolfProUserID = userInfo.user.id,
                    DateRegistered = userInfo.user.created_at
                };

            }
            catch (Exception ex)
            {
                throw ex;
            }

            return user;
        }

        /// <summary>
        /// Get identity service user info by user auth token
        /// </summary>
        /// <param name="userToken"></param>
        /// <returns></returns>
        private User GetIdentityUserInfoByToken(string userToken)
        {
            User user = null;
            try
            {
                user = IdentityContext.Users.FirstOrDefault(s => s.AuthToken == userToken);
            }
            catch(Exception ex)
            {
                throw ex;
            }

            return user;
        }

        #endregion


    }
}
