﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace GolfProService.Identity.Models.DTO.Signin
{
    public class UserRoleDTO
    {
        [Required]
        public long userid { get; set; }
        [Required]
        public long role { get; set; }
    }
}
