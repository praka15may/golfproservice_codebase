﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GolfProService.Identity.Models.DTO.Signin
{
    public class UserInfoDTO
    {
        public int id { get; set; }
        public DateTime created_at { get; set; }
    }
}
