﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GolfProService.Identity.Models.DTO.Signin
{
    public class UserDTO
    {
        public UserInfoDTO user { get; set; }
    }
}
