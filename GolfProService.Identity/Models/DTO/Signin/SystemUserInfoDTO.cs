﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GolfProService.Identity.Models.DTO.Signin
{
    public class SystemUserInfoDTO
    {
        public bool tagged { get; set; }
        public string token { get; set; }
        public long? role { get; set; }
        public long userid { get; set; }
    }
}
