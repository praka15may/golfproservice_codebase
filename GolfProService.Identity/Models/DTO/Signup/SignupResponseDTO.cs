﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GolfProService.Identity.Models.DTO.Signup
{
    public class SignupResponseDTO
    {
         public string Token { get; set; }
         public string Confirmation_URL { get; set; }
       
    }
}
