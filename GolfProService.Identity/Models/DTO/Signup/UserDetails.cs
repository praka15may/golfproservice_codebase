﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GolfProService.Identity.Models.DTO.Signup
{
    public class UserDetails
    {
        public string firstname { get; set; }
        public string lastname { get; set; }
        public string email { get; set; }
        public string zip { get; set; }
        public string password { get; set; }
        public int role { get; set; }
    }
}
