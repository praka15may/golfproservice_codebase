﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GolfProService.Identity.Models.DTO.Signup
{
   
    public class SignupRequestDTO
    {
        public string email { get; set; }
        public string password { get; set; }
        public string first_name { get; set; }
        public string last_name { get; set; }
        public string born_on { get; set; }
        public string gender { get; set; }
        public string address_1 { get; set; }
        public string address_city { get; set; }
        public string address_state { get; set; }
        public string address_zipcode { get; set; }
        public string address_country { get; set; }
        public string phone { get; set; }
    }
}
