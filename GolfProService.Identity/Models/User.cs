﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace GolfProService.Identity.Models
{
    public class User
    {
        public long UserID { get; set; }
        public long GolfProUserID { get; set; }
        public long? UserTypeID { get; set; }
        [ForeignKey("UserTypeID")]
        public virtual UserType UserType { get; set; }
        public string AuthToken { get; set; }
        public DateTime DateRegistered { get; set; }

    }
}
