﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GolfProService.Identity.Models
{
    public class UserType
    {
        public long UserTypeID { get; set; }
        public string TypeOfUser { get; set; }
    }
}
