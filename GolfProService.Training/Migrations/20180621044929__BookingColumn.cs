﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace GolfProService.Training.Migrations
{
    public partial class _BookingColumn : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "AgeOfAttendee",
                table: "TrainingSessionBookings");

            migrationBuilder.AddColumn<double>(
                name: "BookingCost",
                table: "TrainingSessionBookings",
                nullable: false,
                defaultValue: 0.0);

            migrationBuilder.AddColumn<DateTime>(
                name: "BookingDate",
                table: "TrainingSessionBookings",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<long>(
                name: "BookingSlotID",
                table: "TrainingSessionBookings",
                nullable: false,
                defaultValue: 0L);

            migrationBuilder.AddColumn<long>(
                name: "ExpertiseID",
                table: "TrainingSessionBookings",
                nullable: false,
                defaultValue: 0L);

            migrationBuilder.CreateIndex(
                name: "IX_TrainingSessionBookings_BookingSlotID",
                table: "TrainingSessionBookings",
                column: "BookingSlotID");

            migrationBuilder.AddForeignKey(
                name: "FK_TrainingSessionBookings_MasterCodes_BookingSlotID",
                table: "TrainingSessionBookings",
                column: "BookingSlotID",
                principalTable: "MasterCodes",
                principalColumn: "MasterCodeID",
                onDelete: ReferentialAction.NoAction);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_TrainingSessionBookings_MasterCodes_BookingSlotID",
                table: "TrainingSessionBookings");

            migrationBuilder.DropIndex(
                name: "IX_TrainingSessionBookings_BookingSlotID",
                table: "TrainingSessionBookings");

            migrationBuilder.DropColumn(
                name: "BookingCost",
                table: "TrainingSessionBookings");

            migrationBuilder.DropColumn(
                name: "BookingDate",
                table: "TrainingSessionBookings");

            migrationBuilder.DropColumn(
                name: "BookingSlotID",
                table: "TrainingSessionBookings");

            migrationBuilder.DropColumn(
                name: "ExpertiseID",
                table: "TrainingSessionBookings");

            migrationBuilder.AddColumn<string>(
                name: "AgeOfAttendee",
                table: "TrainingSessionBookings",
                nullable: true);
        }
    }
}
