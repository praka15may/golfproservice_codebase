﻿using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace GolfProService.Training.Migrations
{
    public partial class _Initial : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "MasterCodes",
                columns: table => new
                {
                    MasterCodeID = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Code = table.Column<string>(nullable: true),
                    CodeType = table.Column<string>(nullable: true),
                    CreatedByID = table.Column<long>(nullable: true),
                    CreatedDate = table.Column<DateTime>(nullable: false),
                    Label = table.Column<string>(nullable: true),
                    Status = table.Column<string>(nullable: true),
                    UpdatedByID = table.Column<long>(nullable: true),
                    UpdatedDate = table.Column<DateTime>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_MasterCodes", x => x.MasterCodeID);
                });

            migrationBuilder.CreateTable(
                name: "TrainingSessions",
                columns: table => new
                {
                    TrainingSessionID = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    BookingClosed = table.Column<bool>(nullable: false),
                    CreatedByID = table.Column<long>(nullable: true),
                    CreatedDate = table.Column<DateTime>(nullable: false),
                    Date = table.Column<DateTime>(nullable: false),
                    Description = table.Column<string>(nullable: true),
                    Duration = table.Column<int>(nullable: false),
                    EndAt = table.Column<DateTime>(nullable: false),
                    SessionTypeID = table.Column<long>(nullable: false),
                    StartAt = table.Column<DateTime>(nullable: false),
                    Status = table.Column<string>(nullable: true),
                    Title = table.Column<string>(nullable: true),
                    TrainerUserID = table.Column<long>(nullable: false),
                    UpdatedByID = table.Column<long>(nullable: true),
                    UpdatedDate = table.Column<DateTime>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TrainingSessions", x => x.TrainingSessionID);
                    table.ForeignKey(
                        name: "FK_TrainingSessions_MasterCodes_SessionTypeID",
                        column: x => x.SessionTypeID,
                        principalTable: "MasterCodes",
                        principalColumn: "MasterCodeID",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "AllowedAgeLevels",
                columns: table => new
                {
                    AllowedAgeLevelID = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    AgeLevelID = table.Column<long>(nullable: false),
                    CreatedByID = table.Column<long>(nullable: true),
                    CreatedDate = table.Column<DateTime>(nullable: false),
                    Status = table.Column<string>(nullable: true),
                    TrainingSessionID = table.Column<long>(nullable: true),
                    UpdatedByID = table.Column<long>(nullable: true),
                    UpdatedDate = table.Column<DateTime>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AllowedAgeLevels", x => x.AllowedAgeLevelID);
                    table.ForeignKey(
                        name: "FK_AllowedAgeLevels_TrainingSessions_TrainingSessionID",
                        column: x => x.TrainingSessionID,
                        principalTable: "TrainingSessions",
                        principalColumn: "TrainingSessionID",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "AllowedExpertiseLevels",
                columns: table => new
                {
                    AllowedExpertiseLevelID = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    CreatedByID = table.Column<long>(nullable: true),
                    CreatedDate = table.Column<DateTime>(nullable: false),
                    ExpertiseID = table.Column<long>(nullable: false),
                    Status = table.Column<string>(nullable: true),
                    TrainingSessionID = table.Column<long>(nullable: true),
                    UpdatedByID = table.Column<long>(nullable: true),
                    UpdatedDate = table.Column<DateTime>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AllowedExpertiseLevels", x => x.AllowedExpertiseLevelID);
                    table.ForeignKey(
                        name: "FK_AllowedExpertiseLevels_TrainingSessions_TrainingSessionID",
                        column: x => x.TrainingSessionID,
                        principalTable: "TrainingSessions",
                        principalColumn: "TrainingSessionID",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "TrainingSessionBookings",
                columns: table => new
                {
                    TrainingSessionBookingID = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    AgeLevelID = table.Column<long>(nullable: false),
                    AgeOfAttendee = table.Column<string>(nullable: true),
                    BookingStatusID = table.Column<long>(nullable: false),
                    CreatedByID = table.Column<long>(nullable: true),
                    CreatedDate = table.Column<DateTime>(nullable: false),
                    NameOfAttendee = table.Column<string>(nullable: true),
                    PlayerUserID = table.Column<long>(nullable: false),
                    Status = table.Column<string>(nullable: true),
                    TrainingSessionID = table.Column<long>(nullable: true),
                    UpdatedByID = table.Column<long>(nullable: true),
                    UpdatedDate = table.Column<DateTime>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TrainingSessionBookings", x => x.TrainingSessionBookingID);
                    table.ForeignKey(
                        name: "FK_TrainingSessionBookings_MasterCodes_BookingStatusID",
                        column: x => x.BookingStatusID,
                        principalTable: "MasterCodes",
                        principalColumn: "MasterCodeID",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_TrainingSessionBookings_TrainingSessions_TrainingSessionID",
                        column: x => x.TrainingSessionID,
                        principalTable: "TrainingSessions",
                        principalColumn: "TrainingSessionID",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_AllowedAgeLevels_TrainingSessionID",
                table: "AllowedAgeLevels",
                column: "TrainingSessionID");

            migrationBuilder.CreateIndex(
                name: "IX_AllowedExpertiseLevels_TrainingSessionID",
                table: "AllowedExpertiseLevels",
                column: "TrainingSessionID");

            migrationBuilder.CreateIndex(
                name: "IX_TrainingSessionBookings_BookingStatusID",
                table: "TrainingSessionBookings",
                column: "BookingStatusID");

            migrationBuilder.CreateIndex(
                name: "IX_TrainingSessionBookings_TrainingSessionID",
                table: "TrainingSessionBookings",
                column: "TrainingSessionID");

            migrationBuilder.CreateIndex(
                name: "IX_TrainingSessions_SessionTypeID",
                table: "TrainingSessions",
                column: "SessionTypeID");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "AllowedAgeLevels");

            migrationBuilder.DropTable(
                name: "AllowedExpertiseLevels");

            migrationBuilder.DropTable(
                name: "TrainingSessionBookings");

            migrationBuilder.DropTable(
                name: "TrainingSessions");

            migrationBuilder.DropTable(
                name: "MasterCodes");
        }
    }
}
