﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using GolfProService.Training.Models;
using GolfProService.Training.Repository.DapperUtility;
using GolfProService.Training.Repository.EFUtility;
using GolfProService.Training.Repository.MasterDataManagement;
using GolfProService.Training.Repository.TrainingManagement;
using GolfProService.Training.Services.External;
using GolfProService.Training.Services.Utility;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Cors.Internal;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;
using Swashbuckle.AspNetCore.Swagger;

namespace GolfProService.Training
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddMvc();
            //services.AddMvc().AddJsonOptions(options =>
            //{
            //    options.SerializerSettings.DateTimeZoneHandling = DateTimeZoneHandling.Utc;
            //});
            services.AddAutoMapper();

            // Register application services.
            services.AddScoped<ISessionEFRepository, SessionEFRepository>();
            services.AddScoped<IDapperGenericRepository, DapperGenericRepository>();
            services.AddScoped<ISessionDapperRepository, SessionDapperRepository>();
            services.AddScoped<IExternalService, ExternalService>();
            services.AddScoped<IServiceUtility, ServiceUtility>();
            services.AddScoped<IMasterDataEFRepository, MasterDataEFRepository>();

            services.AddCors(options =>
            {
                options.AddPolicy("AllowAny",
                   builder =>
                               builder.AllowAnyOrigin()
                                      .AllowAnyMethod()
                                      .AllowAnyHeader());
            });
            services.Configure<MvcOptions>(options =>
            {
                options.Filters.Add(new CorsAuthorizationFilterFactory("AllowAny"));
            });

            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new Info { Title = "GolfPro API", Version = "v1" });
            });
            services.AddDbContext<TrainingContext>(options => options.UseSqlServer(Configuration.GetConnectionString("TrainingDatabase")));
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            app.UseSwagger();

            // Enable middleware to serve swagger-ui (HTML, JS, CSS, etc.), specifying the Swagger JSON endpoint.
            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("/swagger/v1/swagger.json", "GolfPro API v1");
            });
            app.UseCors("AllowAny");
            app.UseMvc();
        }
    }
}
