﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace GolfProService.Training.Repository.EFUtility
{
    public interface IEFGenericRepository<T> where T : class
    {
        Task<IEnumerable<T>> GetAllAsync();
        IEnumerable<T> GetAll();

        IEnumerable<T> GetAll(string includeProperties = "");
        Task<IEnumerable<T>> GetAllAsync(string includeProperties = "");

        Task<IEnumerable<T>> GetAsync(Expression<Func<T, bool>> predicate);
        IEnumerable<T> Get(Expression<Func<T, bool>> predicate);

        IEnumerable<T> Get(Expression<Func<T, bool>> predicate, string includeProperties = "");
        Task<IEnumerable<T>> GetAsync(Expression<Func<T, bool>> predicate, string includeProperties = "");

        T Find(params object[] keys);
        Task<T> FindAsync(params object[] keys);

        Task<T> FindAsync(Expression<Func<T, bool>> predicate);
        T Find(Expression<Func<T, bool>> predicate);

        Task<T> FindAsync(Expression<Func<T, bool>> predicate, string includeProperties = "");
        T Find(Expression<Func<T, bool>> predicate, string includeProperties = "");

        Task<T> FindAsync(Expression<Func<T, bool>> predicate, params string[] includeProperties);
        T Find(Expression<Func<T, bool>> predicate, params string[] includeProperties);

        bool Any(Expression<Func<T, bool>> predicate);
        Task<bool> AnyAsync(Expression<Func<T, bool>> predicate);

        T Create(T t);

        void Update(T t);

        IEnumerable<T> UpdateRange(IEnumerable<T> tObjects);

        void Delete(object id);

        void Delete(T t);

        Task<int> SaveAsync();
        int Save();

        Task<int> CountAsync();
        int Count();
    }
}
