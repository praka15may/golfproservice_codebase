﻿using Dapper;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;

namespace GolfProService.Training.Repository.DapperUtility
{
    internal class DapperGenericRepository : IDapperGenericRepository
    {
        private IConfiguration Configuration { get; }
        private string connectionString;
        public DapperGenericRepository(IConfiguration configuration)
        {
            Configuration = configuration;
            connectionString = Configuration.GetConnectionString("TrainingDatabase");
        }
        protected IDbConnection OpenConnection
        {
            get  {
                return new SqlConnection(connectionString);
            }
        }

        public List<T> ExecuteStoredProcedure<T>(string spName, DynamicParameters DP)
        {
            using (IDbConnection connection = OpenConnection)
            {
                connection.Open();
                var result = connection.Query<T>(spName, DP, commandType: CommandType.StoredProcedure);
                return result.ToList();
            }
        }

        public SqlMapper.GridReader QueryMultiple(string Query, DynamicParameters dp)
        {
            using (IDbConnection connection = OpenConnection)
            {
                connection.Open();
                SqlMapper.GridReader result = connection.QueryMultiple(Query, param: dp, commandType: CommandType.StoredProcedure);
                return result;
            }
        }

        public async Task<T> QueryMultipleAsync<T>(string Query, Func<SqlMapper.GridReader, T> ObjectMapping, bool isStoredProcedure, DynamicParameters dp = null)
        {
            using (IDbConnection connection = OpenConnection)
            {
                SqlMapper.GridReader result = await connection.QueryMultipleAsync(Query, param: dp, commandType: isStoredProcedure ? CommandType.StoredProcedure : CommandType.Text);
                return ObjectMapping(result);
            }
        }
    }
}
