﻿using Dapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GolfProService.Training.Repository.DapperUtility
{
    public interface IDapperGenericRepository
    {
        List<T> ExecuteStoredProcedure<T>(string spName, DynamicParameters DP);
        SqlMapper.GridReader QueryMultiple(string Query, DynamicParameters dp);
        Task<T> QueryMultipleAsync<T>(string Query, Func<SqlMapper.GridReader, T> ObjectMapping, bool isStoredProcedure, DynamicParameters dp = null);
    }
}
