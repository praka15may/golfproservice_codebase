﻿using GolfProService.Training.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GolfProService.Training.Repository.MasterDataManagement
{
    public interface IMasterDataEFRepository
    {
        MasterCode GetIndividualSession();
        List<MasterCode> GetAllSessionType();
        List<MasterCode> GetAllBookingStatus();
    }
}
