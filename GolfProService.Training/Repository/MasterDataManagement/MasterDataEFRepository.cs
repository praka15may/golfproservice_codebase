﻿using AutoMapper;
using GolfProService.Training.Models;
using GolfProService.Training.Models.DTO;
using GolfProService.Training.Repository.EFUtility;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GolfProService.Training.Repository.MasterDataManagement
{
    internal class MasterDataEFRepository : EFGenericRepository<MasterCode>, IMasterDataEFRepository
    {
        private readonly IMapper mapper = null;
        public MasterDataEFRepository(TrainingContext context, IMapper Mapper) : base(context)
        {
            mapper = Mapper;
        }

        public List<MasterCode> GetAllBookingStatus()
        {
            List<MasterCode> sessionType = null;
            try
            {
                sessionType = Get(s => s.CodeType == "SessionType").ToList();
            }
            catch(Exception ex)
            {
                throw ex;
            }
            return sessionType;
        }

        public List<MasterCode> GetAllSessionType()
        {
            List<MasterCode> bookingStatus = null;
            try
            {
                bookingStatus = Get(s => s.CodeType == "BookingStatus").ToList();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return bookingStatus;
        }

        public MasterCode GetIndividualSession()
        {
            MasterCode individualSession = null;
            try
            {
                individualSession = Find(s => s.CodeType == "SessionType" && s.Code.Contains("Individual"));
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return individualSession;
        }        
    }
}
