﻿using GolfProService.Training.Models;
using GolfProService.Training.Models.DTO;
using GolfProService.Training.Repository.EFUtility;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GolfProService.Training.Repository.TrainingManagement
{
    public interface ISessionEFRepository : IEFGenericRepository<TrainingSession>
    {
        TrainingSessionDTO CreateTrainingSession(TrainingSessionDTO trainingSession);
        Task<TrainingSessionDTO> GetProSessionByID(long sessionID);
        TrainingSessionDTO UpdateTrainingSession(TrainingSession trainingSession);
        bool RemoveTrainingSessionByID(long sessionID);
        Task<TrainingSessionBookingDTO> NewBookingForSessionByID(long sessionID, TrainingSessionBookingDTO bookingInfo);
    }
}
