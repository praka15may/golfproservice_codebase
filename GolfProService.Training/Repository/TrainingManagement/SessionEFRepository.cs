﻿using AutoMapper;
using GolfProService.Training.Infrastructure.Exceptions;
using GolfProService.Training.Models;
using GolfProService.Training.Models.DTO;
using GolfProService.Training.Models.Enum;
using GolfProService.Training.Repository.EFUtility;
using GolfProService.Training.Repository.MasterDataManagement;
using GolfProService.Training.Services.External;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;

namespace GolfProService.Training.Repository.TrainingManagement
{
    internal class SessionEFRepository : EFGenericRepository<TrainingSession>, ISessionEFRepository
    {
        private readonly IMapper mapper = null;
        ISessionDapperRepository sessionDapperRepository = null;
        IMasterDataEFRepository masterDataEFRepository = null;
        private IExternalService externalService = null;
        public SessionEFRepository(TrainingContext context, IMapper Mapper, ISessionDapperRepository SessionDapperRepository, IMasterDataEFRepository MasterDataEFRepository, IExternalService ExternalService) : base(context)
        {
            mapper = Mapper;
            sessionDapperRepository = SessionDapperRepository;
            masterDataEFRepository = MasterDataEFRepository;
            externalService = ExternalService;
        }
        public TrainingSessionDTO CreateTrainingSession(TrainingSessionDTO trainingSession)
        {
            TrainingSession session = null;
            try
            {
                session = mapper.Map<TrainingSessionDTO, TrainingSession>(trainingSession, session);
                session.Duration = (session.EndAt - session.StartAt).Minutes;
                Create(session);
                Save();
                trainingSession = mapper.Map<TrainingSession, TrainingSessionDTO>(session, trainingSession);
            }
            catch(Exception ex)
            {
                throw ex;
            }
            return trainingSession;
        }

        public TrainingSessionDTO UpdateTrainingSession(TrainingSession newSession)
        {
            TrainingSessionDTO updatedSessionInfo = null;
            TrainingSession oldSession = null;
            try
            {
                oldSession = GetSessionDetails(newSession.TrainingSessionID);
                if(oldSession != null)
                {
                    oldSession = Mapper.Map<TrainingSession, TrainingSession>(newSession, oldSession);
                    UpdateAllowedAgeLevel(newSession, oldSession);
                    UpdateAllowedExpertise(newSession, oldSession);                    
                    oldSession.Duration = (oldSession.EndAt - oldSession.StartAt).Minutes;                    
                    Update(oldSession);
                    Save();
                    oldSession.AllowedAgeLevels = oldSession.AllowedAgeLevels.Where(a => a.Status != StatusType.Inactive.ToString()).ToList();
                    oldSession.AllowedExpertiseLevels = oldSession.AllowedExpertiseLevels.Where(a => a.Status != StatusType.Inactive.ToString()).ToList();
                    updatedSessionInfo = Mapper.Map<TrainingSession, TrainingSessionDTO>(oldSession, updatedSessionInfo);
                }
                else
                {
                    throw new SessionNotFoundException();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return updatedSessionInfo;
        }
        
        public async Task<TrainingSessionDTO> GetProSessionByID(long sessionID)
        {
            TrainingSessionDTO session = null;
            TrainingSession trainingSession = null;
            
            try
            {
                var masterResult = await sessionDapperRepository.GetAgeLevelAndExpertiseMasterData();
                trainingSession = GetSessionDetails(sessionID);
                if (trainingSession != null)
                {
                    trainingSession.AllowedAgeLevels = trainingSession.AllowedAgeLevels.Where(a => a.Status != StatusType.Inactive.ToString()).ToList();
                    trainingSession.AllowedExpertiseLevels = trainingSession.AllowedExpertiseLevels.Where(a => a.Status != StatusType.Inactive.ToString()).ToList();
                    session = Mapper.Map<TrainingSession, TrainingSessionDTO>(trainingSession, session);
                    session = MapMasterDataWithSession(session, masterResult);
                }
                else
                {
                    throw new SessionNotFoundException();
                }
                
            }
            catch(Exception ex)
            {
                throw ex;
            }

            return session;
        }

        public bool RemoveTrainingSessionByID(long sessionID)
        {
            bool IsRemoved = false;
            try
            {
                TrainingSession session = Find(sessionID);
                if(session != null)
                {
                    session.Status = StatusType.Inactive.ToString();
                    Update(session);
                    Save();
                    IsRemoved = true;
                }
                else
                {
                    throw new SessionNotFoundException();
                }

            }           
            catch(Exception ex)
            {
                throw ex;
            }

            return IsRemoved;
        }

        public async Task<TrainingSessionBookingDTO> NewBookingForSessionByID(long sessionID, TrainingSessionBookingDTO bookingInfo)
        {
            TrainingSessionBooking booking = null;
            try
            {
               booking = Mapper.Map<TrainingSessionBookingDTO, TrainingSessionBooking>(bookingInfo, booking);
               TrainingSession session = GetSessionAndBookingDetails(sessionID);
                if(session != null)
                {
                    if (await IsAgeRangeAllowed(bookingInfo.PlayerUserID, session))
                    {
                        if (IsIndividualSession(session.SessionTypeID))
                            session.BookingClosed = true;
                        else
                            session.BookingClosed = false;

                        booking.Status = StatusType.Active.ToString();
                        booking.BookingCost = session.Cost;
                        booking.BookingDate = DateTime.UtcNow;
                        session.TrainingSessionBookings.Add(booking);
                        Update(session);
                        Save();
                        bookingInfo = Mapper.Map<TrainingSessionBooking, TrainingSessionBookingDTO>(session.TrainingSessionBookings.Last(), bookingInfo);
                    }
                    else
                    {
                        throw new AllowedAgeRangeException();
                    }
                    
                }
                else
                {
                    throw new SessionNotFoundException();
                }
               
            }
            catch(Exception ex)
            {
                throw ex;
            }
            return bookingInfo;
        }

        #region Private Methods

        private TrainingSession GetSessionDetails(long sessionID)
        {
            TrainingSession session = null;
            try
            {
                var includeProperties = new string[]
                 {
                                "SessionType",
                                "AllowedAgeLevels",
                                "AllowedExpertiseLevels"
                 };
                session = Find(s => s.TrainingSessionID == sessionID, includeProperties);
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return session;
        }

        private TrainingSession GetSessionAndBookingDetails(long sessionID)
        {
            TrainingSession session = null;
            try
            {
                var includeProperties = new string[]
                 {
                     "TrainingSessionBookings",
                     "TrainingSessionBookings.BookingStatus",
                     "TrainingSessionBookings.BookingSlot",
                     "AllowedAgeLevels",
                     "AllowedExpertiseLevels"
                 };
                session = Find(s => s.TrainingSessionID == sessionID, includeProperties);
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return session;
        }

        private static void UpdateAllowedExpertise(TrainingSession newSession,TrainingSession oldSession)
        {
            try
            {
                foreach (var expertise in newSession.AllowedExpertiseLevels)
                {
                    if (expertise.AllowedExpertiseLevelID != 0)
                    {
                        var expert = oldSession.AllowedExpertiseLevels.FirstOrDefault(e => e.AllowedExpertiseLevelID == expertise.AllowedExpertiseLevelID);
                        if (expert != null)
                            expert = Mapper.Map<AllowedExpertiseLevel, AllowedExpertiseLevel>(expertise, expert);
                    }
                    else
                        oldSession.AllowedExpertiseLevels.Add(expertise);
                }

                
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private static void UpdateAllowedAgeLevel(TrainingSession newSession,TrainingSession oldSession)
        {
            try
            {
                foreach (var ageLevel in newSession.AllowedAgeLevels)
                {
                    if (ageLevel.AllowedAgeLevelID != 0)
                    {
                        var level = oldSession.AllowedAgeLevels.FirstOrDefault(a => a.AllowedAgeLevelID == ageLevel.AllowedAgeLevelID);
                        if (level != null)
                            level = Mapper.Map<AllowedAgeLevel, AllowedAgeLevel>(ageLevel, level);
                    }
                    else
                        oldSession.AllowedAgeLevels.Add(ageLevel);
                }                
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private TrainingSessionDTO MapMasterDataWithSession(TrainingSessionDTO session, object masterResult)
        {
            List<AgeLevelDTO> ageLevelList = new List<AgeLevelDTO>();
            List<ExpertiseDTO> expertiseList = new List<ExpertiseDTO>();
            try
            {
                DeseralizeMasterData(masterResult, out ageLevelList, out expertiseList);

                if(session.AllowedAgeLevels.Count > 0)
                {
                    foreach (var allowedLevel in session.AllowedAgeLevels)
                    {
                        foreach (var level in ageLevelList)
                        {
                            if (allowedLevel.AgeLevelID == level.AgeLevelID)
                            {
                                allowedLevel.Level = level.Level;
                                break;
                            }

                        }
                    }
                }
                
                if(session.AllowedExpertiseLevels.Count > 0)
                {
                    foreach (var allowedExpert in session.AllowedExpertiseLevels)
                    {
                        foreach (var expert in expertiseList)
                        {
                            if (allowedExpert.ExpertiseID == expert.ExpertiseID)
                            {
                                allowedExpert.Name = expert.Name;
                                break;
                            }

                        }
                    }
                }                

            }
            catch (Exception ex)
            {
                throw ex;
            }
            return session;
        }

        private static void DeseralizeMasterData(object masterResult, out List<AgeLevelDTO> ageLevelList, out List<ExpertiseDTO> expertiseList)
        {
            try
            {
                Type t = masterResult.GetType();
                PropertyInfo prop1 = t.GetProperty("AgeLevels");
                object AgeLevels = prop1.GetValue(masterResult);
                PropertyInfo prop2 = t.GetProperty("Expertise");
                object Expertise = prop2.GetValue(masterResult);

                var ageLevel = JsonConvert.SerializeObject(AgeLevels);
                ageLevelList = JsonConvert.DeserializeObject<List<AgeLevelDTO>>(ageLevel);
                var expert = JsonConvert.SerializeObject(Expertise);
                expertiseList = JsonConvert.DeserializeObject<List<ExpertiseDTO>>(expert);
            }
            catch(Exception ex)
            {
                throw ex;
            }
            
        }
        
        private bool IsIndividualSession(long sessionTypeID)
        {
            MasterCode indvidualSession = null;
            bool IsIndividualSession = false;
            try
            {
                indvidualSession = masterDataEFRepository.GetIndividualSession();
                if (indvidualSession.MasterCodeID == sessionTypeID)
                    IsIndividualSession = true;

            }
            catch(Exception ex)
            {
                throw ex;
            }
            return IsIndividualSession;
        }

        private async Task<UserInfoDTO> GetUserInfoByUserToken(string userToken)
        {
            UserDTO userInfo = null;
            try
            {
                userInfo = await externalService.GetUserInfoByUserToken(userToken);
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return userInfo.UserInfo;
        }

        private async Task<bool> IsAgeRangeAllowed(long playerUserID, TrainingSession trainingSession)
        {
            UserIdentityInfoDTO userIdentity = null;
            UserInfoDTO userInfo = null;
            List<AgeLevelDTO> allowedAgeRanges = null;
            List<AgeLevelDTO> ageLevelList = new List<AgeLevelDTO>();
            List<ExpertiseDTO> expertiseList = new List<ExpertiseDTO>();
            bool IsAgeRangeAllowed = false;
            try
            {
                userIdentity = sessionDapperRepository.GetUserIdentityInfoByID(playerUserID);
                if(userIdentity != null)
                {
                    userInfo = await GetUserInfoByUserToken(userIdentity.AuthToken);

                    if (userInfo != null)
                    {
                        var masterResult = await sessionDapperRepository.GetAgeLevelAndExpertiseMasterData();
                        DeseralizeMasterData(masterResult, out ageLevelList, out expertiseList);

                        allowedAgeRanges = new List<AgeLevelDTO>();
                        foreach (var age in ageLevelList)
                        {
                            foreach (var allowedAge in trainingSession.AllowedAgeLevels)
                            {
                                if (allowedAge.AgeLevelID == age.AgeLevelID)
                                    allowedAgeRanges.Add(age);
                            }
                        }

                        if (allowedAgeRanges.Any(a => a.Level.Contains(AgeRangeType.Any.ToString())))
                            IsAgeRangeAllowed = true;
                        else if (userInfo.Age <= 18 && allowedAgeRanges.Any(a => a.Level == AgeRangeType.Junior.ToString()))
                            IsAgeRangeAllowed = true;
                        else if (userInfo.Age >= 19 && userInfo.Age <= 54 && allowedAgeRanges.Any(a => a.Level == AgeRangeType.Adult.ToString()))
                            IsAgeRangeAllowed = true;
                        else if (userInfo.Age > 54 && allowedAgeRanges.Any(a => a.Level == AgeRangeType.Senior.ToString()))
                            IsAgeRangeAllowed = true;

                    }
                    else
                    {
                        throw new UserNotFoundException();
                    }
                }
                else
                {
                    throw new UserNotFoundException();
                }
                

            }
            catch (Exception ex)
            {
                throw ex;
            }
            return IsAgeRangeAllowed;
        }

        #endregion
    }
}
