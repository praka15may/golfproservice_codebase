﻿using GolfProService.Training.Models.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GolfProService.Training.Repository.TrainingManagement
{
    public interface ISessionDapperRepository
    {
        List<SessionInfoDTO> GetAllCurrentProSessionByID(int userID, DateTime currentDate);
        List<SessionInfoDTO> GetAllPastProSessionByID(int userID);
        List<SessionInfoDTO> GetAllUpcomingProSessionByID(int userID);
        Task<object> GetAgeLevelAndExpertiseMasterData();
        List<SessionBookingsListDTO> GetAllSessionBookingsByID(long sessionID);
        Task<List<BookingInfoDTO>> GetAllPlayerUpcomingBookingsByID(long playerUserID);
        Task<List<BookingInfoDTO>> GetAllPlayerPastBookingsByID(long playerUserID);
        UserIdentityInfoDTO GetUserIdentityInfoByID(long playerUserID);
    }
}
