﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Dynamic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper.Configuration;
using Dapper;
using GolfProService.Training.Models.DTO;
using GolfProService.Training.Repository.DapperUtility;
using GolfProService.Training.Services.External;

namespace GolfProService.Training.Repository.TrainingManagement
{
    public class SessionDapperRepository : ISessionDapperRepository
    {
        //Initializing the dapper repository
        private IDapperGenericRepository dapperRepository = null;
        private IExternalService externalService = null;
        public SessionDapperRepository(IDapperGenericRepository DapperRepository, IExternalService ExternalService)
        {
            dapperRepository = DapperRepository;
            externalService = ExternalService;
        }

        public async Task<object> GetAgeLevelAndExpertiseMasterData()
        {
            try
            {
                var result = await dapperRepository.QueryMultipleAsync("spGetAgeLevelAndExpertiseMasterData", (reader) =>
                {
                    var AgeLevels = reader.Read<AgeLevelDTO>();
                    var Expertise = reader.Read<ExpertiseDTO>();
                    return new { AgeLevels, Expertise };
                }, true);
                return result;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<SessionInfoDTO> GetAllCurrentProSessionByID(int userID, DateTime currentDate)
        {
            List<SessionInfoDTO> sessionList = null;
            try
            {
                DynamicParameters parameters = new DynamicParameters();
                parameters.Add("@UserID", userID);
                parameters.Add("@Date", currentDate);
                sessionList = dapperRepository.ExecuteStoredProcedure<SessionInfoDTO>("spGetAllCurrentProSession", parameters).ToList();
            }
            catch(Exception ex)
            {
                throw ex;
            }
            return sessionList;
        }

        public List<SessionInfoDTO> GetAllPastProSessionByID(int userID)
        {
            List<SessionInfoDTO> sessionList = null;
            try
            {
                DynamicParameters parameters = new DynamicParameters();
                parameters.Add("@UserID", userID);
                parameters.Add("@Date", DateTime.UtcNow);
                sessionList = dapperRepository.ExecuteStoredProcedure<SessionInfoDTO>("spGetAllPastProSession", parameters).ToList();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return sessionList;
        }

        public List<SessionBookingsListDTO> GetAllSessionBookingsByID(long sessionID)
        {
            List<SessionBookingsListDTO> sessionBookings = null;
            try
            {
                DynamicParameters parameters = new DynamicParameters();
                parameters.Add("@SessionID", sessionID);
                sessionBookings = dapperRepository.ExecuteStoredProcedure<SessionBookingsListDTO>("spGetAllSessionBookingsByID", parameters).ToList();
            }
            catch(Exception ex)
            {
                throw ex;
            }

            return sessionBookings;
        }

        public async Task<List<BookingInfoDTO>> GetAllPlayerUpcomingBookingsByID(long playerUserID)
        {
            List<BookingInfoDTO> sessionBookings = null;
            try
            {
                sessionBookings = GetPlayerBookingsData("spGetAllPlayerUpcomingBookingsByID", playerUserID);
                if (sessionBookings.Count > 0)
                    sessionBookings = await MapUserInformation(sessionBookings);

            }
            catch(Exception ex)
            {
                throw ex;
            }
            return sessionBookings;
        }        

        public async Task<List<BookingInfoDTO>> GetAllPlayerPastBookingsByID(long playerUserID)
        {
            List<BookingInfoDTO> sessionBookings = null;
           
            try
            {
                sessionBookings = GetPlayerBookingsData("spGetAllPlayerPastBookingsByID", playerUserID);
                if(sessionBookings.Count > 0)
                    sessionBookings = await MapUserInformation(sessionBookings);
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return sessionBookings;
        }        

        public List<SessionInfoDTO> GetAllUpcomingProSessionByID(int userID)
        {
            List<SessionInfoDTO> sessionList = null;
            try
            {
                DynamicParameters parameters = new DynamicParameters();
                parameters.Add("@UserID", userID);
                parameters.Add("@Date", DateTime.UtcNow);
                sessionList = dapperRepository.ExecuteStoredProcedure<SessionInfoDTO>("spGetAllUpcomingProSession", parameters).ToList();
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return sessionList;
        }

        public UserIdentityInfoDTO GetUserIdentityInfoByID(long playerUserID)
        {
            UserIdentityInfoDTO identityInfo = null;
            try
            {
                DynamicParameters parameters = new DynamicParameters();
                parameters.Add("@UserID", playerUserID);;
                identityInfo = dapperRepository.ExecuteStoredProcedure<UserIdentityInfoDTO>("spGetUserIdentityInfoByID", parameters).FirstOrDefault();
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return identityInfo;
        }

        #region Private Methods

        private List<BookingInfoDTO> GetPlayerBookingsData(string spName, long playerUserID)
        {
            List<BookingInfoDTO> sessionBookings;
            try
            {
                DynamicParameters parameters = new DynamicParameters();
                parameters.Add("@PlayerUserID", playerUserID);
                parameters.Add("@Date", DateTime.UtcNow);
                sessionBookings = dapperRepository.ExecuteStoredProcedure<BookingInfoDTO>(spName, parameters).ToList();
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return sessionBookings;
        }

        private async Task<UserInfoDTO> GetUserInfoByUserToken(string userToken)
        {
            UserDTO userInfo = null;
            try
            {
                userInfo = await externalService.GetUserInfoByUserToken(userToken);
            }
            catch(Exception ex)
            {
                throw ex;
            }

            return userInfo.UserInfo;
        }

        private async Task<List<BookingInfoDTO>> MapUserInformation(List<BookingInfoDTO> sessionBookings)
        {
            UserInfoDTO userInfo = null;
            {
                try
                {
                    foreach (var booking in sessionBookings)
                    {
                        if (booking.TrainerAuthToken != null)
                            userInfo = await GetUserInfoByUserToken(booking.TrainerAuthToken);

                        if (userInfo != null)
                            booking.TrainerName = userInfo.FirstName + " " + userInfo.LastName;
                    }
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }


            return sessionBookings;
        }
        
        #endregion


    }
}
