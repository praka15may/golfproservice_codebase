﻿using GolfProService.Training.Infrastructure.Exceptions;
using GolfProService.Training.Models.DTO;
using GolfProService.Training.Services.Utility;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GolfProService.Training.Services.External
{
    internal class ExternalService : IExternalService
    {
        IServiceUtility ServiceUtility = null;
        public ExternalService(IServiceUtility serviceUtility)
        {
            ServiceUtility = serviceUtility;
        }

        /// <summary>
        /// Make call to marketplace api service to get User info
        /// </summary>
        /// <param name="userToken"></param>
        /// <returns></returns>
        public async Task<UserDTO> GetUserInfoByUserToken(string userToken)
        {
            UserDTO userInfo = null;
            try
            {
                userInfo = await ServiceUtility.GetDataFromService<UserDTO>("/api/v4/users?details=n", "MarketplaceStagingAPIs", userToken);
            }
            catch (UserNotAuthorizedException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return userInfo;
        }
    }
}
