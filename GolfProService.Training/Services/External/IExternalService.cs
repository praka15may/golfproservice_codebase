﻿using GolfProService.Training.Models.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GolfProService.Training.Services.External
{
    public interface IExternalService
    {
        Task<UserDTO> GetUserInfoByUserToken(string userToken);
    }
}
