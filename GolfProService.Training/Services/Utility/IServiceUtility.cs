﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GolfProService.Training.Services.Utility
{
    public interface IServiceUtility
    {
        Task<TObject> GetDataFromService<TObject>(string URL, string serviceName, string userToken) where TObject : new();
        Task<TObject> PostDataToService<TObject>(string URL, string serviceName, string userToken, object data) where TObject : new();
        Task<TObject> PutDataToService<TObject>(string URL, string serviceName, string userToken, object data) where TObject : new();
    }
}
