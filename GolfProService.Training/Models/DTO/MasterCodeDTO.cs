﻿using Newtonsoft.Json;

namespace GolfProService.Training.Models.DTO
{
    public class MasterCodeDTO
    {
        [JsonProperty("mastercodeid")]
        public long MasterCodeID { get; set; }
        [JsonProperty("codetype")]
        public string CodeType { get; set; }
        [JsonProperty("code")]
        public string Code { get; set; }
        [JsonProperty("label")]
        public string Label { get; set; }
        [JsonProperty("status")]
        public string Status { get; set; }
    }
}