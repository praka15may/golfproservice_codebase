﻿using Newtonsoft.Json;
using System;
using System.ComponentModel.DataAnnotations;

namespace GolfProService.Training.Models.DTO
{
    public class TrainingSessionBookingDTO
    {
        [JsonProperty("trainingsessionbookingid")]
        public long TrainingSessionBookingID { get; set; }
        [Required]
        [JsonProperty("playeruserid")]
        public long PlayerUserID { get; set; }
        [Required]
        [JsonProperty("bookingstatusid")]
        public long BookingStatusID { get; set; }
        [JsonProperty("bookingstatus")]
        public MasterCodeDTO BookingStatus { get; set; }
        [Required]
        [JsonProperty("bookingslotid")]
        public long BookingSlotID { get; set; }
        [JsonProperty("bookingslot")]
        public MasterCodeDTO BookingSlot { get; set; }
        [Required]
        [JsonProperty("bookingcost")]
        public double BookingCost { get; set; }
        [Required]
        [JsonProperty("bookingdate")]
        public DateTime BookingDate { get; set; }
        [Required]
        [JsonProperty("agelevelid")]
        public long AgeLevelID { get; set; }
        [Required]
        [JsonProperty("expertiseid")]
        public long ExpertiseID { get; set; }
        [Required]
        [JsonProperty("nameofattendee")]
        public string NameOfAttendee { get; set; }
        [JsonProperty("status")]
        public string Status { get; set; }
    }
}