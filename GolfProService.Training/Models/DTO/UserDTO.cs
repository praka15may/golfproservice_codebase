﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GolfProService.Training.Models.DTO
{
    public class UserDTO
    {
        [JsonProperty("user")]
        public UserInfoDTO UserInfo { get; set; }
    }
}
