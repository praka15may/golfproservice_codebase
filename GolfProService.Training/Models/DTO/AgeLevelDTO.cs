﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GolfProService.Training.Models.DTO
{
    public class AgeLevelDTO
    {
        [JsonProperty("id")]
        public int AgeLevelID { get; set; }
        [JsonProperty("level")]
        public string Level { get; set; }
        [JsonProperty("description")]
        public string Description { get; set; }
        [JsonProperty("status")]
        public string Status { get; set; }
    }
}
