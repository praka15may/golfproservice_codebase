﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace GolfProService.Training.Models.DTO
{
    public class TrainingSessionDTO
    {
        [JsonProperty("trainingsessionid")]
        public long TrainingSessionID { get; set; }
        [Required]
        [JsonProperty("traineruserid")]
        public long TrainerUserID { get; set; }
        [Required]
        [JsonProperty("sessiontypeid")]
        public long SessionTypeID { get; set; }
        [JsonProperty("sessiontype")]
        public MasterCodeDTO SessionType { get; set; }
        [JsonProperty("title")]
        public string Title { get; set; }
        [JsonProperty("description")]
        public string Description { get; set; }
        [Required]
        [JsonProperty("cost")]
        public double Cost { get; set; }
        [Required]
        [JsonProperty("date")]
        public DateTime Date { get; set; }
        [Required]
        [JsonProperty("startat")]
        public DateTime StartAt { get; set; }
        [Required]
        [JsonProperty("endat")]
        public DateTime EndAt { get; set; }
        [JsonProperty("duration")]
        public int Duration { get; set; }
        [JsonProperty("bookingclosed")]
        [Required]
        public bool BookingClosed { get; set; }
        [JsonProperty("allowedagelevels")]
        public List<AllowedAgeLevelDTO> AllowedAgeLevels { get; set; }
        [JsonProperty("allowedexpertiselevels")]
        public List<AllowedExpertiseLevelDTO> AllowedExpertiseLevels { get; set; }
        [JsonProperty("status")]
        public string Status { get; set; }
    }
}
