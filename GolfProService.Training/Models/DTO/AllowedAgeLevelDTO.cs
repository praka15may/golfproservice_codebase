﻿using Newtonsoft.Json;
using System.ComponentModel.DataAnnotations;

namespace GolfProService.Training.Models.DTO
{
    public class AllowedAgeLevelDTO
    {
        [JsonProperty("allowedagelevelid")]
        public long AllowedAgeLevelID { get; set; }
        [Required]
        [JsonProperty("agelevelid")]
        public long AgeLevelID { get; set; }
        [JsonProperty("agelevel")]
        public string Level { get; set; }
        [JsonProperty("status")]
        public string Status { get; set; }
    }
}