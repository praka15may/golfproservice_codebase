﻿using Newtonsoft.Json;
using System.ComponentModel.DataAnnotations;

namespace GolfProService.Training.Models.DTO
{
    public class AllowedExpertiseLevelDTO
    {
        [JsonProperty("allowedexpertiselevelid")]
        public long AllowedExpertiseLevelID { get; set; }
        [Required]
        [JsonProperty("expertiseid")]
        public long ExpertiseID { get; set; }
        [JsonProperty("expertise")]
        public string Name { get; set; }
        [JsonProperty("status")]
        public string Status { get; set; }
    }
}