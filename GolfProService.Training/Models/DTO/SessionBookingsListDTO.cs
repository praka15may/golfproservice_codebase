﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GolfProService.Training.Models.DTO
{
    public class SessionBookingsListDTO
    {
        [JsonProperty("trainingsessionbookingid")]
        public long TrainingSessionBookingID { get; set; }
        [JsonProperty("trainingsessionid")]
        public long TrainingSessionID { get; set; }
        [JsonProperty("traineruserid")]
        public long TrainerUserID { get; set; }
        [JsonProperty("title")]
        public string Title { get; set; }
        [JsonProperty("description")]
        public string Description { get; set; }
        [JsonProperty("sessiontype")]
        public string SessionType { get; set; }
        [JsonProperty("playeruserid")]
        public long PlayerUserID { get; set; }
        [JsonProperty("bookingdate")]
        public DateTime BookingDate { get; set; }
        [JsonProperty("trainingdate")]
        public DateTime Date { get; set; }
        [JsonProperty("startat")]
        public DateTime StartAt { get; set; }
        [JsonProperty("endat")]
        public DateTime EndAt { get; set; }
        [JsonProperty("duration")]
        public int Duration { get; set; }
        [JsonProperty("agelevel")]
        public string AgeLevel { get; set; }
        [JsonProperty("expertise")]
        public string Expertise { get; set; }
        [JsonProperty("nameofattendee")]
        public string NameOfAttendee { get; set; }
        [JsonProperty("status")]
        public string Status { get; set; }
    }
}
