﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace GolfProService.Training.Models.DTO
{
    public class UserIdentityInfoDTO
    {
        public long UserID { get; set; }
        public string AuthToken { get; set; }
        public DateTime DateRegistered { get; set; }
        public long GolfProUserID { get; set; }
        public long UserTypeID { get; set; }
    }
}
