﻿using GolfProService.Training.Models.SystemEntities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace GolfProService.Training.Models
{
    public class TrainingSession : AuditableEntity
    {
        [Key]
        public long TrainingSessionID { get; set; }
        public long TrainerUserID { get; set; }
        public long SessionTypeID { get; set; }
        [ForeignKey("SessionTypeID")]
        public virtual MasterCode SessionType { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public double Cost { get; set; }
        public DateTime Date { get; set; }
        public DateTime StartAt { get; set; }
        public DateTime EndAt { get; set; }
        public int Duration { get; set; }
        public bool BookingClosed { get; set; }
        public virtual List<AllowedAgeLevel> AllowedAgeLevels { get; set; }
        public virtual List<AllowedExpertiseLevel> AllowedExpertiseLevels { get; set; }
        public virtual List<TrainingSessionBooking> TrainingSessionBookings { get; set; }
        public string Status { get; set; }
    }
}
