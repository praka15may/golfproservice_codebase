﻿using GolfProService.Training.Models.SystemEntities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace GolfProService.Training.Models
{
    public class TrainingSessionBooking : AuditableEntity
    {
        [Key]
        public long TrainingSessionBookingID { get; set; }
        public long PlayerUserID { get; set; }
        public long BookingStatusID { get; set; }
        [ForeignKey("BookingStatusID")]
        public virtual MasterCode BookingStatus { get; set; }
        public long BookingSlotID { get; set; }
        [ForeignKey("BookingSlotID")]
        public virtual MasterCode BookingSlot { get; set; }
        public double BookingCost { get; set; }
        public DateTime BookingDate { get; set; }
        public long AgeLevelID { get; set; }
        public long ExpertiseID { get; set; }
        public string NameOfAttendee { get; set; }
        public string Status { get; set; }
    }
}
