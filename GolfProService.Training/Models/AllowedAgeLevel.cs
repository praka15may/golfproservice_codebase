﻿using GolfProService.Training.Models.SystemEntities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace GolfProService.Training.Models
{
    public class AllowedAgeLevel : AuditableEntity
    {
        [Key]
        public long AllowedAgeLevelID { get; set; }
        public long AgeLevelID { get; set; }
        public string Status { get; set; }
    }
}
