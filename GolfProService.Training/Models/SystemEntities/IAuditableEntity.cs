﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GolfProService.Training.Models.SystemEntities
{
    public interface IAuditableEntity
    {
        DateTime UpdatedDate { get; set; }
        DateTime CreatedDate { get; set; }
        long? UpdatedByID { get; set; }
        long? CreatedByID { get; set; }
    }
}
