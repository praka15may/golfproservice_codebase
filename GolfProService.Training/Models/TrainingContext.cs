﻿using GolfProService.Training.Models.SystemEntities;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GolfProService.Training.Models
{
    public class TrainingContext : DbContext
    {
        public TrainingContext(DbContextOptions<TrainingContext> options) : base(options) { }
        public virtual DbSet<TrainingSession> TrainingSessions { get; set; }
        public virtual DbSet<TrainingSessionBooking> TrainingSessionBookings { get; set; }
        public virtual DbSet<AllowedAgeLevel> AllowedAgeLevels { get; set; }
        public virtual DbSet<AllowedExpertiseLevel> AllowedExpertiseLevels { get; set; }
        public virtual DbSet<MasterCode> MasterCodes { get; set; }

        /// <summary>
        /// Add/update the system entity values for tracking
        /// </summary>
        /// <returns></returns>
        public override int SaveChanges()
        {
            var modifiedEntries = ChangeTracker.Entries()
                .Where(x => x.Entity is IAuditableEntity
                    && (x.State == EntityState.Added || x.State == EntityState.Modified));

            foreach (var entry in modifiedEntries)
            {
                IAuditableEntity entity = entry.Entity as IAuditableEntity;
                if (entity != null)
                {
                    DateTime now = DateTime.UtcNow;

                    if (entry.State == EntityState.Added)
                    {
                        entity.CreatedDate = now;
                    }
                    else
                    {
                        base.Entry(entity).Property(x => x.CreatedDate).IsModified = false;
                    }
                    entity.UpdatedDate = now;
                }
            }

            return base.SaveChanges();
        }
    }
}
