﻿using GolfProService.Training.Models.SystemEntities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace GolfProService.Training.Models
{
    public class AllowedExpertiseLevel : AuditableEntity
    {
        [Key]
        public long AllowedExpertiseLevelID { get; set; }
        public long ExpertiseID { get; set; }
        public string Status { get; set; }
    }
}
