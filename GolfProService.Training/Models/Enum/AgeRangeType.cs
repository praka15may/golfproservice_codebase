﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GolfProService.Training.Models.Enum
{
    public enum AgeRangeType
    {
        Adult=1,
        Junior,
        Senior,
        Any
    }
}
