﻿using GolfProService.Training.Models.SystemEntities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace GolfProService.Training.Models
{
    public class MasterCode : AuditableEntity
    {
        [Key]
        public long MasterCodeID { get; set; }
        public string CodeType { get; set; }
        public string Code { get; set; }
        public string Label { get; set; }
        public string Status { get; set; }
    }
}
