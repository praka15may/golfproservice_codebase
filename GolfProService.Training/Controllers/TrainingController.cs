﻿using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using GolfProService.Training.Infrastructure.Exceptions;
using GolfProService.Training.Infrastructure.Message;
using GolfProService.Training.Models;
using GolfProService.Training.Models.DTO;
using GolfProService.Training.Repository.TrainingManagement;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace GolfProService.Training.Controllers
{   
    [Route("api/Training/[Action]")]
    public class TrainingController : Controller
    {
        private readonly IMapper mapper = null;
        private readonly ISessionEFRepository trainingRepository = null;
        private readonly ISessionDapperRepository trainingDapperRepository = null;
        public TrainingController(IMapper Mapper,ISessionEFRepository TrainingRepository, ISessionDapperRepository TrainingDapperRepository)
        {
            mapper = Mapper;
            trainingRepository = TrainingRepository;
            trainingDapperRepository = TrainingDapperRepository;
        }

        [HttpPost]
        [ProducesResponseType(200, Type = typeof(TrainingSessionDTO))]
        public IActionResult CreateTrainingSession([FromBody]TrainingSessionDTO trainingSession)
        {
            dynamic obj = new ExpandoObject();
            try
            {
                if (ModelState.IsValid)
                {
                    obj.Result = trainingRepository.CreateTrainingSession(trainingSession);
                }
                else
                {
                    // Returns validation errors by comma seprated
                    obj.Title = ExceptionMessage.DataValidation_Title;
                    obj.error = String.Join(", ", ModelState.Keys.SelectMany(key => this.ModelState[key].Errors.Select(x => key + ": " + x.ErrorMessage)));
                    return BadRequest(obj);
                }
                
            }
            catch(Exception ex)
            {
                obj.ErrorTitle = ExceptionMessage.InternalError_Title;
                obj.ErrorMessage = ExceptionMessage.InternalError_Message;
                return BadRequest(obj);
            }
            return Ok(obj.Result);
        }

        [HttpPut]
        [ProducesResponseType(200, Type = typeof(TrainingSessionDTO))]
        public IActionResult UpdateTrainingSession([FromBody]TrainingSessionDTO trainingSession)
        {
            dynamic obj = new ExpandoObject();
            TrainingSession session = null;
            try
            {
                if (ModelState.IsValid)
                {
                    session = mapper.Map<TrainingSessionDTO, TrainingSession>(trainingSession, session);
                    obj.Result = trainingRepository.UpdateTrainingSession(session);
                }
                else
                {
                    // Returns validation errors by comma seprated
                    obj.Title = ExceptionMessage.DataValidation_Title;
                    obj.error = String.Join(", ", ModelState.Keys.SelectMany(key => this.ModelState[key].Errors.Select(x => key + ": " + x.ErrorMessage)));
                    return BadRequest(obj);
                }
            }
            catch (SessionNotFoundException)
            {
                obj.ErrorTitle = ExceptionMessage.SessionNotFound_Title;
                obj.ErrorMessage = ExceptionMessage.SessionNotFound_Message;
                return BadRequest(obj);
            }
            catch (Exception ex)
            {
                obj.ErrorTitle = ExceptionMessage.InternalError_Title;
                obj.ErrorMessage = ExceptionMessage.InternalError_Message;
                return BadRequest(obj);
            }
            return Ok(obj.Result);
        }

        [HttpGet]
        [ProducesResponseType(200, Type = typeof(SessionInfoDTO))]
        public IActionResult GetAllProSessionByDate(int userID, DateTime date)
        {
            dynamic obj = new ExpandoObject();
            try
            {
                obj.Result = trainingDapperRepository.GetAllCurrentProSessionByID(userID, date);
            }
            catch(Exception ex)
            {
                obj.ErrorTitle = ExceptionMessage.InternalError_Title;
                obj.ErrorMessage = ExceptionMessage.InternalError_Message;
                return BadRequest(obj);
            }

            return Ok(obj.Result);
        }

        [HttpGet]
        [ProducesResponseType(200, Type = typeof(SessionInfoDTO))]
        public IActionResult GetAllPastProSession(int userID)
        {
            dynamic obj = new ExpandoObject();
            try
            {
                obj.Result = trainingDapperRepository.GetAllPastProSessionByID(userID);
            }
            catch (Exception ex)
            {
                obj.ErrorTitle = ExceptionMessage.InternalError_Title;
                obj.ErrorMessage = ExceptionMessage.InternalError_Message;
                return BadRequest(obj);
            }

            return Ok(obj.Result);
        }

        [HttpGet]
        [ProducesResponseType(200, Type = typeof(SessionInfoDTO))]
        public IActionResult GetAllUpcomingProSession(int userID)
        {
            dynamic obj = new ExpandoObject();
            try
            {
                obj.Result = trainingDapperRepository.GetAllUpcomingProSessionByID(userID);
            }
            catch (Exception ex)
            {
                obj.ErrorTitle = ExceptionMessage.InternalError_Title;
                obj.ErrorMessage = ExceptionMessage.InternalError_Message;
                return BadRequest(obj);
            }

            return Ok(obj.Result);
        }

        [HttpGet]
        [ProducesResponseType(200, Type = typeof(TrainingSessionDTO))]
        public async Task<IActionResult> GetProSessionByID(long sessionID)
        {
            dynamic obj = new ExpandoObject();
            try
            {
                obj.Result = await trainingRepository.GetProSessionByID(sessionID);
            }
            catch(SessionNotFoundException)
            {
                obj.ErrorTitle = ExceptionMessage.SessionNotFound_Title;
                obj.ErrorMessage = ExceptionMessage.SessionNotFound_Message;
                return BadRequest(obj);
            }
            catch (Exception ex)
            {
                obj.ErrorTitle = ExceptionMessage.InternalError_Title;
                obj.ErrorMessage = ExceptionMessage.InternalError_Message;
                return BadRequest(obj);
            }

            return Ok(obj.Result);
        }

        [HttpPut]
        [ProducesResponseType(200, Type = typeof(bool))]
        public IActionResult RemoveTrainingSessionByID(long sessionID)
        {
            dynamic obj = new ExpandoObject();
            try
            {
                return Ok(trainingRepository.RemoveTrainingSessionByID(sessionID));
            }
            catch (SessionNotFoundException)
            {
                obj.ErrorTitle = ExceptionMessage.SessionNotFound_Title;
                obj.ErrorMessage = ExceptionMessage.SessionNotFound_Message;
                return BadRequest(obj);
            }
            catch (Exception ex)
            {
                obj.ErrorTitle = ExceptionMessage.InternalError_Title;
                obj.ErrorMessage = ExceptionMessage.InternalError_Message;
                return BadRequest(obj);
            }
        }

        [HttpPost]
        [ProducesResponseType(200, Type = typeof(TrainingSessionBookingDTO))]
        public async Task<IActionResult> NewBookingForSessionByID(long sessionID, [FromBody]TrainingSessionBookingDTO booking)
        {
            dynamic obj = new ExpandoObject();
            try
            {
                return Ok(await trainingRepository.NewBookingForSessionByID(sessionID, booking));
            }
            catch (SessionNotFoundException)
            {
                obj.ErrorTitle = ExceptionMessage.SessionNotFound_Title;
                obj.ErrorMessage = ExceptionMessage.SessionNotFound_Message;
                return BadRequest(obj);
            }
            catch (UserNotFoundException)
            {
                obj.ErrorTitle = ExceptionMessage.PlayerNotFound_Title;
                obj.ErrorMessage = ExceptionMessage.PlayerNotFound_Message;
                return BadRequest(obj);
            }
            catch (AllowedAgeRangeException)
            {
                obj.ErrorTitle = ExceptionMessage.AgeRangeNotAllowed_Title;
                obj.ErrorMessage = ExceptionMessage.AgeRangeNotAllowed_Message;
                return BadRequest(obj);
            }
            catch (Exception ex)
            {
                obj.ErrorTitle = ExceptionMessage.InternalError_Title;
                obj.ErrorMessage = ExceptionMessage.InternalError_Message;
                return BadRequest(obj);
            }
        }

        [HttpGet]
        [ProducesResponseType(200, Type = typeof(SessionBookingsListDTO))]
        public IActionResult GetAllSessionBookingsByID(long sessionID)
        {
            dynamic obj = new ExpandoObject();
            try
            {
                obj.Result = trainingDapperRepository.GetAllSessionBookingsByID(sessionID);
            }
            catch (Exception ex)
            {
                obj.ErrorTitle = ExceptionMessage.InternalError_Title;
                obj.ErrorMessage = ExceptionMessage.InternalError_Message;
                return BadRequest(obj);
            }

            return Ok(obj.Result);
        }

        [HttpGet]
        [ProducesResponseType(200, Type = typeof(BookingInfoDTO))]
        public async Task<IActionResult> GetAllPlayerUpcomingBookingsByID(long playerUserID)
        {
            dynamic obj = new ExpandoObject();
            try
            {
                obj.Result = await trainingDapperRepository.GetAllPlayerUpcomingBookingsByID(playerUserID);
            }
            catch (Exception ex)
            {
                obj.ErrorTitle = ExceptionMessage.InternalError_Title;
                obj.ErrorMessage = ExceptionMessage.InternalError_Message;
                return BadRequest(obj);
            }

            return Ok(obj.Result);
        }

        [HttpGet]
        [ProducesResponseType(200, Type = typeof(BookingInfoDTO))]
        public async Task<IActionResult> GetAllPlayerPastBookingsByID(long playerUserID)
        {
            dynamic obj = new ExpandoObject();
            try
            {
                obj.Result = await trainingDapperRepository.GetAllPlayerPastBookingsByID(playerUserID);
            }
            catch (Exception ex)
            {
                obj.ErrorTitle = ExceptionMessage.InternalError_Title;
                obj.ErrorMessage = ExceptionMessage.InternalError_Message;
                return BadRequest(obj);
            }

            return Ok(obj.Result);
        }
    }
}