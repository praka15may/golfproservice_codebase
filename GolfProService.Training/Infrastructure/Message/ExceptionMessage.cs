﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GolfProService.Training.Infrastructure.Message
{
    public static class ExceptionMessage
    {
        public static string DataValidation_Title = "Data Validation Error";
        public static string InternalError_Title = "Internal Server Error";
        public static string InternalError_Message = "Sorry for Inconvenience !!!! Please Try Again Later...";
        public static string SessionNotFound_Title = "Session Not Found";
        public static string SessionNotFound_Message = "No session found for the given id";
        public static string AgeRangeNotAllowed_Title = "Age Range Not Allowed";
        public static string AgeRangeNotAllowed_Message = "Please select the session allowed age range to continue the booking";
        public static string PlayerNotFound_Title = "Player Not Found";
        public static string PlayerNotFound_Message = "No player found for the given id";

    }
}
