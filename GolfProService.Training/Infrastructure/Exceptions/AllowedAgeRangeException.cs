﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GolfProService.Training.Infrastructure.Exceptions
{
    public class AllowedAgeRangeException : ApplicationException
    {
        public AllowedAgeRangeException()
        {
        }
        public AllowedAgeRangeException(string message) : base(message)
        {
        }

        public AllowedAgeRangeException(string message, Exception innerException) : base(message, innerException)
        {
        }
    }
}
