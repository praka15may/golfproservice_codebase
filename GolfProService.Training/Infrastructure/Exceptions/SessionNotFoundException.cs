﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GolfProService.Training.Infrastructure.Exceptions
{
    public class SessionNotFoundException : ApplicationException
    {
        public SessionNotFoundException()
        {
        }
        public SessionNotFoundException(string message) : base(message)
        {
        }

        public SessionNotFoundException(string message, Exception innerException) : base(message, innerException)
        {
        }
    }
}
