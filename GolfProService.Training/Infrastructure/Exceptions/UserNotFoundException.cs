﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GolfProService.Training.Infrastructure.Exceptions
{
    public class UserNotFoundException : ApplicationException
    {
        public UserNotFoundException()
        {
        }
        public UserNotFoundException(string message) : base(message)
        {
        }

        public UserNotFoundException(string message, Exception innerException) : base(message, innerException)
        {
        }
    }
}
