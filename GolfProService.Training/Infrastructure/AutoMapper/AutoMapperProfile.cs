﻿using AutoMapper;
using GolfProService.Training.Models;
using GolfProService.Training.Models.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GolfProService.Training.Infrastructure.AutoMapper
{
    public class AutoMapperProfile : Profile
    {
        public AutoMapperProfile()
        {
            CreateMap<TrainingSessionDTO, TrainingSession>()
                .ForMember(x => x.SessionType, opt => opt.Ignore());
            CreateMap<TrainingSession, TrainingSessionDTO>();
            CreateMap<TrainingSession, TrainingSession>()
                .ForMember(x => x.SessionType, opt => opt.Ignore())
                .ForMember(x => x.AllowedAgeLevels, opt => opt.Ignore())
                .ForMember(x => x.AllowedExpertiseLevels, opt => opt.Ignore()); 

            CreateMap<TrainingSessionBookingDTO, TrainingSessionBooking>()
                .ForMember(x => x.BookingStatus, opt => opt.Ignore())
                .ForMember(x => x.BookingSlot, opt => opt.Ignore());
            CreateMap<TrainingSessionBooking, TrainingSessionBookingDTO>();
            CreateMap<TrainingSessionBooking, TrainingSessionBooking>();

            CreateMap<MasterCodeDTO, MasterCode>();
            CreateMap<MasterCode, MasterCodeDTO>();
            CreateMap<MasterCode, MasterCode>();

            CreateMap<AllowedExpertiseLevelDTO, AllowedExpertiseLevel>();
            CreateMap<AllowedExpertiseLevel, AllowedExpertiseLevelDTO>();
            CreateMap<AllowedExpertiseLevel, AllowedExpertiseLevel>();

            CreateMap<AllowedAgeLevelDTO, AllowedAgeLevel>();
            CreateMap<AllowedAgeLevel, AllowedAgeLevelDTO>();
            CreateMap<AllowedAgeLevel, AllowedAgeLevel>();
        }
    }
}
