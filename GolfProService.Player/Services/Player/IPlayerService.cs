﻿using GolfProService.Player.Models.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GolfProService.Player.Services.Player
{
    public interface IPlayerService
    {
        Task<object> UpdateProfile(PlayerDetails playerInfo, string usertoken);
        Task<PlayerDetails> GetPlayerProfileInfo(string userToken);
    }
}
