﻿using GolfProService.Player.Models.DTO;
using GolfProService.Player.Services.Utility;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GolfProService.Player.Services.Player
{
    public class PlayerService : IPlayerService
    {
        private IServiceUtility utilityService = null;
        public PlayerService(IServiceUtility utilityService)
        {
            this.utilityService = utilityService;
        }
        public async Task<object> UpdateProfile(PlayerDetails playerInfo, string usertoken)
        {
            object response = false;
            try
            {
                response = await utilityService.PutDataToService<object>("/api/v4/users", "MarketplaceStagingAPIs", usertoken, CreateFormattedPlayer(playerInfo));
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return response;
        }

        public async Task<PlayerDetails> GetPlayerProfileInfo(string userToken)
        {
            PlayerInfo playerInfo = null;
            PlayerDetails playerDetail = null;
            try
            {
                playerInfo = await utilityService.GetDataFromService<PlayerInfo>("/api/v4/users?details=n", "MarketplaceStagingAPIs", userToken);
                if (playerInfo.user != null)
                    playerDetail = CreatePlayerprofileObject(playerInfo.user);
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return playerDetail;
        }

        private PlayerProfileInfo CreateFormattedPlayer(PlayerDetails playerDetails)
        {
            PlayerProfileInfo player = null;
            try
            {
                player = new PlayerProfileInfo();
                player.email = playerDetails.email;
                player.first_name = playerDetails.first_name;
                player.last_name = playerDetails.last_name;
                player.phone = playerDetails.phonenumber;
                player.gender = playerDetails.gender;
                player.avatar = playerDetails.avatar;
                player.born_on = playerDetails.dob;
                if (playerDetails.address != null)
                {
                    player.address_1 = playerDetails.address.address1;
                    player.address_city = playerDetails.address.city;
                    player.address_country = playerDetails.address.country;
                    player.address_state = playerDetails.address.state;
                    player.address_zipcode = playerDetails.address.zip;
                }
              
                if (playerDetails.playerstats != null)
                {
                    player.skill_level = playerDetails.playerstats.skilllevel;
                    //if(playerDetails.playerstats.dob!=null)
                    //player.born_on = playerDetails.playerstats.dob.year + playerDetails.playerstats.dob.month + playerDetails.playerstats.dob.day;
                }

            }
            catch (Exception ex)
            {
                throw ex;
            }

            return player;
        }

        private PlayerDetails CreatePlayerprofileObject(PlayerProfileInfo playerProfileInfo)
        {
            PlayerDetails playerDetail = null;
            try
            {
                playerDetail = new PlayerDetails();
                playerDetail.id = playerProfileInfo.id;
                playerDetail.first_name = playerProfileInfo.first_name;
                playerDetail.last_name = playerProfileInfo.last_name;
                playerDetail.email = playerProfileInfo.email;
                playerDetail.phonenumber = playerProfileInfo.phone;
                playerDetail.gender = playerProfileInfo.gender;
                playerDetail.avatar_url = playerProfileInfo.avatar_url;
                playerDetail.dob = playerProfileInfo.born_on;
                playerDetail.age = playerProfileInfo.age;
                playerDetail.address = new Address();
                playerDetail.address.address1 = playerProfileInfo.address_1;
                playerDetail.address.city = playerProfileInfo.address_city;
                playerDetail.address.state = playerProfileInfo.address_state;
                playerDetail.address.country = playerProfileInfo.address_country;
                playerDetail.address.zip = playerProfileInfo.address_zipcode;
                playerDetail.playerstats = new PlayerStats();
            }
            catch(Exception ex)
            {
                throw ex;
            }

            return playerDetail;
        }

    }
}
