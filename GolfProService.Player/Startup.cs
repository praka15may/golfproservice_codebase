﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using GolfProService.Player.Infrastructure.CustomHelpers;
using GolfProService.Player.Infrastructure.Filters;
using GolfProService.Player.Models;
using GolfProService.Player.Repository.Player;
using GolfProService.Player.Services.Player;
using GolfProService.Player.Services.Utility;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Cors.Internal;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Microsoft.Extensions.PlatformAbstractions;
using Swashbuckle.AspNetCore.Swagger;

namespace GolfProService.Player
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddMvc();
            // Register application services.
            services.AddScoped<IServiceUtility, ServiceUtility>();
            services.AddScoped<IPlayerService, PlayerService>();
            services.AddScoped<IPlayerRepository, PlayerRepository>();
            //var basePath = PlatformServices.Default.Application.ApplicationBasePath;
            //var xmlPath = Path.Combine(basePath, "GolfProService.Player.xml");
            services.AddCors(options =>
            {
                options.AddPolicy("AllowAny",
                   builder =>
                               builder.AllowAnyOrigin()
                                      .AllowAnyMethod()
                                      .AllowAnyHeader());
            });
            services.Configure<MvcOptions>(options =>
            {
                options.Filters.Add(new CorsAuthorizationFilterFactory("AllowAny"));
            });
            // Register the Swagger generator, defining one or more Swagger documents
            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new Info { Title = "GolfPro API", Version = "v1" });
                //c.IncludeXmlComments(xmlPath);
                c.OperationFilter<AddUserTokenHeaderParameter>();
            });
            services.AddDbContext<PlayerContext>(options => options.UseSqlServer(Configuration.GetConnectionString("PlayerDatabase")));

            //services.AddMvc(option =>
            //{
            //    // add our custom binder to beginning of collection
            //    option.ModelBinderProviders.Insert(0, new DateTimeModelBinderProvider());
            //});
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseSwagger();

            // Enable middleware to serve swagger-ui (HTML, JS, CSS, etc.), specifying the Swagger JSON endpoint.
            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("/swagger/v1/swagger.json", "GolfPro API v1");
            });
            app.UseCors("AllowAny");
            app.UseMvc();
        }
    }
}
