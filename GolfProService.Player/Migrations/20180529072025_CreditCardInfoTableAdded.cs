﻿using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace GolfProService.Player.Migrations
{
    public partial class CreditCardInfoTableAdded : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "GolfProUserID",
                table: "PlayerProfiles",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.CreateTable(
                name: "CreditCardInfos",
                columns: table => new
                {
                    CreditCardInfoID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    CCRefID = table.Column<int>(nullable: false),
                    CardType = table.Column<string>(nullable: true),
                    CreatedDate = table.Column<DateTime>(nullable: false),
                    HashedCCNo = table.Column<string>(nullable: true),
                    PlayerProfileID = table.Column<int>(nullable: true),
                    UpdatedDate = table.Column<DateTime>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CreditCardInfos", x => x.CreditCardInfoID);
                    table.ForeignKey(
                        name: "FK_CreditCardInfos_PlayerProfiles_PlayerProfileID",
                        column: x => x.PlayerProfileID,
                        principalTable: "PlayerProfiles",
                        principalColumn: "PlayerProfileID",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_CreditCardInfos_PlayerProfileID",
                table: "CreditCardInfos",
                column: "PlayerProfileID");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "CreditCardInfos");

            migrationBuilder.DropColumn(
                name: "GolfProUserID",
                table: "PlayerProfiles");
        }
    }
}
