﻿using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace GolfProService.Player.Migrations
{
    public partial class ExpertiseTableRemoved : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_PlayerProfiles_Expertises_ExpertiseID",
                table: "PlayerProfiles");

            migrationBuilder.DropTable(
                name: "Expertises");

            migrationBuilder.DropIndex(
                name: "IX_PlayerProfiles_ExpertiseID",
                table: "PlayerProfiles");

            migrationBuilder.DropColumn(
                name: "ExpertiseID",
                table: "PlayerProfiles");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "ExpertiseID",
                table: "PlayerProfiles",
                nullable: true);

            migrationBuilder.CreateTable(
                name: "Expertises",
                columns: table => new
                {
                    ExpertiseID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Description = table.Column<string>(nullable: true),
                    Name = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Expertises", x => x.ExpertiseID);
                });

            migrationBuilder.CreateIndex(
                name: "IX_PlayerProfiles_ExpertiseID",
                table: "PlayerProfiles",
                column: "ExpertiseID");

            migrationBuilder.AddForeignKey(
                name: "FK_PlayerProfiles_Expertises_ExpertiseID",
                table: "PlayerProfiles",
                column: "ExpertiseID",
                principalTable: "Expertises",
                principalColumn: "ExpertiseID",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
