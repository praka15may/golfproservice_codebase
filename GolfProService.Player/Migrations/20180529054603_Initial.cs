﻿using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace GolfProService.Player.Migrations
{
    public partial class Initial : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Expertises",
                columns: table => new
                {
                    ExpertiseID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Description = table.Column<string>(nullable: true),
                    Name = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Expertises", x => x.ExpertiseID);
                });

            migrationBuilder.CreateTable(
                name: "PlayerProfiles",
                columns: table => new
                {
                    PlayerProfileID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    ActivationDate = table.Column<DateTime>(nullable: false),
                    DeactivationDate = table.Column<DateTime>(nullable: false),
                    ExpertiseID = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PlayerProfiles", x => x.PlayerProfileID);
                    table.ForeignKey(
                        name: "FK_PlayerProfiles_Expertises_ExpertiseID",
                        column: x => x.ExpertiseID,
                        principalTable: "Expertises",
                        principalColumn: "ExpertiseID",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "DeviceInfos",
                columns: table => new
                {
                    DeviceInfoID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Android_Token = table.Column<string>(nullable: true),
                    Device_Platform = table.Column<string>(nullable: true),
                    Device_Type = table.Column<string>(nullable: true),
                    IOS_Token = table.Column<string>(nullable: true),
                    PlayerProfileID = table.Column<int>(nullable: true),
                    UA_Token = table.Column<string>(nullable: true),
                    UUID = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_DeviceInfos", x => x.DeviceInfoID);
                    table.ForeignKey(
                        name: "FK_DeviceInfos_PlayerProfiles_PlayerProfileID",
                        column: x => x.PlayerProfileID,
                        principalTable: "PlayerProfiles",
                        principalColumn: "PlayerProfileID",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_DeviceInfos_PlayerProfileID",
                table: "DeviceInfos",
                column: "PlayerProfileID");

            migrationBuilder.CreateIndex(
                name: "IX_PlayerProfiles_ExpertiseID",
                table: "PlayerProfiles",
                column: "ExpertiseID");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "DeviceInfos");

            migrationBuilder.DropTable(
                name: "PlayerProfiles");

            migrationBuilder.DropTable(
                name: "Expertises");
        }
    }
}
