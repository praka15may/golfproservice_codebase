﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace GolfProService.Player.Migrations
{
    public partial class DateRegisteredColumnAdded : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "ActivationDate",
                table: "PlayerProfiles");

            migrationBuilder.RenameColumn(
                name: "DeactivationDate",
                table: "PlayerProfiles",
                newName: "DateRegistered");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "DateRegistered",
                table: "PlayerProfiles",
                newName: "DeactivationDate");

            migrationBuilder.AddColumn<DateTime>(
                name: "ActivationDate",
                table: "PlayerProfiles",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));
        }
    }
}
