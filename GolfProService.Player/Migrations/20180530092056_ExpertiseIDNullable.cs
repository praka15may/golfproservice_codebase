﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace GolfProService.Player.Migrations
{
    public partial class ExpertiseIDNullable : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_PlayerProfiles_Expertises_ExpertiseID",
                table: "PlayerProfiles");

            migrationBuilder.AlterColumn<int>(
                name: "ExpertiseID",
                table: "PlayerProfiles",
                nullable: true,
                oldClrType: typeof(int));

            migrationBuilder.AddForeignKey(
                name: "FK_PlayerProfiles_Expertises_ExpertiseID",
                table: "PlayerProfiles",
                column: "ExpertiseID",
                principalTable: "Expertises",
                principalColumn: "ExpertiseID",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_PlayerProfiles_Expertises_ExpertiseID",
                table: "PlayerProfiles");

            migrationBuilder.AlterColumn<int>(
                name: "ExpertiseID",
                table: "PlayerProfiles",
                nullable: false,
                oldClrType: typeof(int),
                oldNullable: true);

            migrationBuilder.AddForeignKey(
                name: "FK_PlayerProfiles_Expertises_ExpertiseID",
                table: "PlayerProfiles",
                column: "ExpertiseID",
                principalTable: "Expertises",
                principalColumn: "ExpertiseID",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
