﻿// <auto-generated />
using GolfProService.Player.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using Microsoft.EntityFrameworkCore.Storage;
using Microsoft.EntityFrameworkCore.Storage.Internal;
using System;

namespace GolfProService.Player.Migrations
{
    [DbContext(typeof(PlayerContext))]
    [Migration("20180530095054_ExpertiseTableRemoved")]
    partial class ExpertiseTableRemoved
    {
        protected override void BuildTargetModel(ModelBuilder modelBuilder)
        {
#pragma warning disable 612, 618
            modelBuilder
                .HasAnnotation("ProductVersion", "2.0.3-rtm-10026")
                .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

            modelBuilder.Entity("GolfProService.Player.Models.CreditCardInfo", b =>
                {
                    b.Property<int>("CreditCardInfoID")
                        .ValueGeneratedOnAdd();

                    b.Property<int>("CCRefID");

                    b.Property<string>("CardType");

                    b.Property<DateTime>("CreatedDate");

                    b.Property<string>("HashedCCNo");

                    b.Property<int?>("PlayerProfileID");

                    b.Property<DateTime>("UpdatedDate");

                    b.HasKey("CreditCardInfoID");

                    b.HasIndex("PlayerProfileID");

                    b.ToTable("CreditCardInfos");
                });

            modelBuilder.Entity("GolfProService.Player.Models.DeviceInfo", b =>
                {
                    b.Property<int>("DeviceInfoID")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("Android_Token");

                    b.Property<string>("Device_Platform");

                    b.Property<string>("Device_Type");

                    b.Property<string>("IOS_Token");

                    b.Property<int?>("PlayerProfileID");

                    b.Property<string>("UA_Token");

                    b.Property<string>("UUID");

                    b.HasKey("DeviceInfoID");

                    b.HasIndex("PlayerProfileID");

                    b.ToTable("DeviceInfos");
                });

            modelBuilder.Entity("GolfProService.Player.Models.PlayerProfile", b =>
                {
                    b.Property<int>("PlayerProfileID")
                        .ValueGeneratedOnAdd();

                    b.Property<DateTime>("ActivationDate");

                    b.Property<DateTime>("DeactivationDate");

                    b.Property<int>("GolfProUserID");

                    b.HasKey("PlayerProfileID");

                    b.ToTable("PlayerProfiles");
                });

            modelBuilder.Entity("GolfProService.Player.Models.CreditCardInfo", b =>
                {
                    b.HasOne("GolfProService.Player.Models.PlayerProfile")
                        .WithMany("CreditCardInfos")
                        .HasForeignKey("PlayerProfileID");
                });

            modelBuilder.Entity("GolfProService.Player.Models.DeviceInfo", b =>
                {
                    b.HasOne("GolfProService.Player.Models.PlayerProfile")
                        .WithMany("DeviceInfos")
                        .HasForeignKey("PlayerProfileID");
                });
#pragma warning restore 612, 618
        }
    }
}
