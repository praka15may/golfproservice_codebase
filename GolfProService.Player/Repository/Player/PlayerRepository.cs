﻿using GolfProService.Player.Models;
using GolfProService.Player.Services.Player;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GolfProService.Player.Repository.Player
{
  
    internal class PlayerRepository : IPlayerRepository
    {
        private PlayerContext PlayerContext = null;
        private IPlayerService PlayerService = null;
        public PlayerRepository(PlayerContext playerContext, IPlayerService playerService)
        {
            PlayerContext = playerContext;
            PlayerService = playerService;
        }

        public async Task<PlayerProfile> SavePlayer(PlayerProfile player)
        {
            PlayerProfile savedPlayer = null;
            try
            {
                PlayerContext.PlayerProfiles.Add(player);
                await PlayerContext.SaveChangesAsync();
                savedPlayer = player;
                return savedPlayer;
            }
            catch(Exception ex)
            {
                throw;
            }
            
        }
        public async Task<PlayerProfile> FindPlayer(int playerId)
        {
            try
            {
                return  PlayerContext.PlayerProfiles.Where(f => f.GolfProUserID == playerId).FirstOrDefault();
                
                
            }
            catch (Exception ex)
            {
                throw;
            }

        }

    }
}
