﻿using GolfProService.Player.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GolfProService.Player.Repository.Player
{
    public interface IPlayerRepository
    {
        Task<PlayerProfile> SavePlayer(PlayerProfile player);
        Task<PlayerProfile> FindPlayer(int playerId);
    }
}
