﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GolfProService.Player.Infrastructure.Exceptions
{
    public class UserNotAuthorizedException : ApplicationException
    {
        public UserNotAuthorizedException()
        {
        }
        public UserNotAuthorizedException(string message) : base(message)
        {
        }

        public UserNotAuthorizedException(string message, Exception innerException) : base(message, innerException)
        {
        }
    }
}
