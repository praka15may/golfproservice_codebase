﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GolfProService.Player.Infrastructure.Message
{
    public static class ValidationErrorMessage
    {
        public const string ALPHABETS_SPACE_NUMBER = "Please enter valid {0}. Only alphabets, spaces and numbers accepted.";
        public const string STRING_LENGTH_MAX_MIN = "{0} must be between {2} and {1} characters in length.";
        public const string NUMBER_LENGTH_MAX = "{0} must be {1} numbers in length.";
        public const string INVALID_EMAIL_FORMAT = "Please enter the valid email address.";
        public const string ACCEPT_NUMBER = "Please enter only numbers.";
    }
}
