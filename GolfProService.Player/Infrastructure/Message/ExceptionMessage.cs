﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GolfProService.Player.Infrastructure.Message
{
    public static class ExceptionMessage
    {
        public static string NotAuthorized_Title = "Not Authorized User";
        public static string NotAuthorized_Error = "Please provide valid user token";
        public static string DataValidation_Title = "Data Validation Error";
    }
}
