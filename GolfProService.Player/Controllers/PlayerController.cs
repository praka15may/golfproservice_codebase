﻿using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Linq;
using System.Threading.Tasks;
using GolfProService.Player.Infrastructure.Message;
using GolfProService.Player.Models;
using GolfProService.Player.Models.DTO;
using GolfProService.Player.Repository.Player;
using GolfProService.Player.Services.Player;
using GolfProService.Player.Services.Utility;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;

namespace GolfProService.Player.Controllers
{
    [Route("api/Player/[Action]")]
    public class PlayerController : Controller
    {
        string usertoken = "";
        private IPlayerService playerService = null;
        private IPlayerRepository playerRepository = null;
        public PlayerController(IPlayerService playerService,IPlayerRepository playerRepository)
        {
            this.playerService = playerService;
            this.playerRepository = playerRepository;
        }
        [HttpPut]
        public async Task<object> UpdateProfile([FromBody]PlayerDetails playerInfo)
        {
            dynamic obj = new ExpandoObject();
            try
            {
                usertoken = HttpContext.Request.Headers["X-Api-User-Token"].FirstOrDefault();
                if(usertoken != null)
                {
                    if (ModelState.IsValid)
                    {
                        var res = await playerService.UpdateProfile(playerInfo, usertoken);
                        var player = await playerRepository.FindPlayer(playerInfo.id);
                        if (player == null)
                        {
                            PlayerProfile playerProfile = new PlayerProfile();
                            playerProfile.GolfProUserID = playerInfo.id;
                            playerProfile.DateRegistered = DateTime.Now;
                            var playerAdded = await playerRepository.SavePlayer(playerProfile);
                        }
                        return res;
                    }
                    else
                    {
                        // Returns validation errors by comma seprated
                        obj.Title = ExceptionMessage.DataValidation_Title;
                        obj.Error = String.Join(", ", ModelState.Keys.SelectMany(key => this.ModelState[key].Errors.Select(x => key + ": " + x.ErrorMessage)));
                        return BadRequest(obj);
                    }
                    
                }
                else
                {
                    throw new UnauthorizedAccessException();
                }
                
            }
            catch (UnauthorizedAccessException)
            {
                return Unauthorized();
            }
            catch(Exception ex)
            {
                object msg = JsonConvert.DeserializeObject(ex.Message);
                return BadRequest(msg);
            }
        }

        [HttpGet]
        [ProducesResponseType(200, Type = typeof(PlayerDetails))]
        public async Task<IActionResult> GetPlayerProfileInfo()
        {
            dynamic obj = new ExpandoObject();
            try
            {
                usertoken = HttpContext.Request.Headers["X-Api-User-Token"].FirstOrDefault();
                obj.Result = await playerService.GetPlayerProfileInfo(usertoken);
            }
            catch(Exception ex)
            {
                return BadRequest(ex.Message);
            }

            return Ok(obj.Result);
        }
    }
}