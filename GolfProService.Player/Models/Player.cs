﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace GolfProService.Player.Models
{
    public class Player
    {
        [Key]
        public int UserId { get; set; }
        public int UserProfileId { get; set; }
        [ForeignKey("UserProfileId")]
        public virtual PlayerProfile Profile { get; set; }
        public string Auth_token { get; set; }
        public DateTime DateRegistered { get; set; }
        public string AccountStatus { get; set; }
 
    }
}
