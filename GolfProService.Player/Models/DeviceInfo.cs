﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GolfProService.Player.Models
{
    public class DeviceInfo
    {
        public int DeviceInfoID { get; set; }
        public string UUID { get; set; }
        public string Device_Type { get; set; }
        public string Device_Platform { get; set; }
        public string UA_Token { get; set; }
        public string IOS_Token { get; set; }
        public string Android_Token { get; set; }
    }
}
