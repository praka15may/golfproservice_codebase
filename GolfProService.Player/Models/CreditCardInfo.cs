﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GolfProService.Player.Models
{
    public class CreditCardInfo
    {
        public int CreditCardInfoID { get; set; }
        public int CCRefID { get; set; }
        public string CardType { get; set; }
        public string HashedCCNo { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime UpdatedDate { get; set; }
    }
}
