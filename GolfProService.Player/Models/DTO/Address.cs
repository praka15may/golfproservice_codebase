﻿using GolfProService.Player.Infrastructure.Message;
using GolfProService.Player.Infrastructure.Rules;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace GolfProService.Player.Models.DTO
{
    public class Address
    {
        [Required]
        [StringLength(20, MinimumLength = 2, ErrorMessage = ValidationErrorMessage.STRING_LENGTH_MAX_MIN)]
        public string address1 { get; set; }
        public string address2 { get; set; }
        [Required]
        [StringLength(20, MinimumLength = 2, ErrorMessage = ValidationErrorMessage.STRING_LENGTH_MAX_MIN)]
        public string city { get; set; }
        public string state { get; set; }
        public string country { get; set; }
        [Required]
        [RegularExpression(RegularExpression.FOR_NUMBERS, ErrorMessage = ValidationErrorMessage.ACCEPT_NUMBER)]
        [StringLength(5, MinimumLength = 5, ErrorMessage = ValidationErrorMessage.NUMBER_LENGTH_MAX)]
        public string zip { get; set; }

    }
}
