﻿using GolfProService.Player.Infrastructure.Attributes;
using GolfProService.Player.Infrastructure.Message;
using GolfProService.Player.Infrastructure.Rules;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace GolfProService.Player.Models.DTO
{
    public class PlayerDetails
    {
        [Required]
        public int id { get; set; }
        [Required]
        [RegularExpression(RegularExpression.FOR_ALPHABETS_SPACE_NUMBER, ErrorMessage = ValidationErrorMessage.ALPHABETS_SPACE_NUMBER)]
        [StringLength(15, MinimumLength = 2, ErrorMessage = ValidationErrorMessage.STRING_LENGTH_MAX_MIN)]
        public string first_name { get; set; }
        [Required]
        [RegularExpression(RegularExpression.FOR_ALPHABETS_SPACE_NUMBER, ErrorMessage = ValidationErrorMessage.ALPHABETS_SPACE_NUMBER)]
        [StringLength(15, MinimumLength = 2, ErrorMessage = ValidationErrorMessage.STRING_LENGTH_MAX_MIN)]
        public string last_name { get; set; }
        [Required]
        [RegularExpression(RegularExpression.FOR_INVALID_EMAIL_FORMAT, ErrorMessage = ValidationErrorMessage.INVALID_EMAIL_FORMAT)]
        public string email { get; set; }
        [RegularExpression(RegularExpression.FOR_NUMBERS, ErrorMessage = ValidationErrorMessage.ACCEPT_NUMBER)]
        [StringLength(10, MinimumLength = 10, ErrorMessage = ValidationErrorMessage.NUMBER_LENGTH_MAX)]
        public string phonenumber { get; set; }
        public string gender { get; set; }
        public string avatar { get; set; }
        public string avatar_url { get; set; }
        public string dob { get; set; }
        [DataType(DataType.Date), DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        public DateTime? DateOfBirth { get; set; }
        public int? age { get; set; }
        public Address address { get; set; }
        public PlayerStats playerstats { get; set; }
    }
}
