﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GolfProService.Player.Models.DTO
{
    public class PlayerProfileInfo
    {
        public int id { get; set; }
        public string first_name { get; set; }
        public string last_name { get; set; }
        public string email { get; set; }
        public string phone { get; set; }
        public string born_on { get; set; }
        public string gender { get; set; }
        public string address_1 { get; set; }
        public string address_city { get; set; }
        public string address_state { get; set; }
        public string address_zipcode { get; set; }
        public string address_country { get; set; }
        public string avatar { get; set; }
        public string avatar_url { get; set; }
        public string skill_level { get; set; }
        public int? age { get; set; }
    }
}
