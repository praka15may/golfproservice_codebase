﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GolfProService.Player.Models.DTO
{
    public class PlayerInfo
    {
        public PlayerProfileInfo user { get; set; }
    }
}
