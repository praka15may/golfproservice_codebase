﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GolfProService.Player.Models.DTO
{
    public class SignUpResponse
    {
        public string token { get; set; }
        public string confirmation_url { get; set; }
    }
}
