﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GolfProService.Player.Models.DTO
{
    public class DOB
    {
        public string month { get; set; }
        public string year { get; set; }
        public string day { get; set; }
    }
}
