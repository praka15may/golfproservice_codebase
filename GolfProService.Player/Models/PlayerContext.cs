﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GolfProService.Player.Models
{
    public class PlayerContext : DbContext
    {
        public PlayerContext(DbContextOptions<PlayerContext> options)
: base(options)
        { }
        public virtual DbSet<PlayerProfile> PlayerProfiles { get; set; }
        
        public virtual DbSet<DeviceInfo> DeviceInfos { get; set; }
        public virtual DbSet<CreditCardInfo> CreditCardInfos { get; set; }
    }
    
}
