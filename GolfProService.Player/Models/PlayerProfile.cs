﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace GolfProService.Player.Models
{
    public class PlayerProfile
    {
        [Key]
        public int PlayerProfileID { get; set; }
        public int GolfProUserID { get; set; }
        public virtual List<DeviceInfo> DeviceInfos { get; set; }
        public virtual List<CreditCardInfo> CreditCardInfos { get; set; }
        public DateTime DateRegistered { get; set; }


    }
}
