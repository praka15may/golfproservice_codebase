﻿using GolfProService.Identity.Models.DTO.Signin;
using GolfProService.Identity.Repository.Identity;
using GolfProService.Identity.Services.Identity;
using GolfProService.Identity.Services.Utility;
using System;
using System.Collections.Generic;
using System.Text;
using Unity;
using Xunit;

namespace GolfProService.UnitTest.Identity.Signin
{
    public class IdentityRepositoryTest_Signin
    {
        // Required for UnityInlineData Attribute
        private readonly IUnityContainer Container;
        private readonly IIdentityRepository identityRepository;


        public IdentityRepositoryTest_Signin()
        {
            Container = new UnityContainer();
            Container.RegisterType<IIdentityRepository, IdentityRepository>();
        }

        [Fact]        
        public async void SaveUserInfoExist_WithValidToken_ShouldReturnSavedUserInfo()
        {
            SystemUserInfoDTO userInfo = new SystemUserInfoDTO();
            userInfo.userid = 4834;
            userInfo.role = null;
            userInfo.token = "4834-$2a$10$RFoVArOWq5TcqoJI4zKOEu";
            userInfo.tagged = false;
            string userToken = "4834-$2a$10$RFoVArOWq5TcqoJI4zKOEu";
            SystemUserInfoDTO userInfoResult = await identityRepository.SaveUserInfo(userToken);
            Assert.Equal(userInfo, userInfoResult);
        }
    }
}
