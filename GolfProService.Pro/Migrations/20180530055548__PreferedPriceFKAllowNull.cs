﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace GolfProService.Pro.Migrations
{
    public partial class _PreferedPriceFKAllowNull : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_ProProfiles_PreferredPricing_PreferredPricingID",
                table: "ProProfiles");

            migrationBuilder.AlterColumn<int>(
                name: "PreferredPricingID",
                table: "ProProfiles",
                nullable: true,
                oldClrType: typeof(int));

            migrationBuilder.AddForeignKey(
                name: "FK_ProProfiles_PreferredPricing_PreferredPricingID",
                table: "ProProfiles",
                column: "PreferredPricingID",
                principalTable: "PreferredPricing",
                principalColumn: "PreferredPricingID",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_ProProfiles_PreferredPricing_PreferredPricingID",
                table: "ProProfiles");

            migrationBuilder.AlterColumn<int>(
                name: "PreferredPricingID",
                table: "ProProfiles",
                nullable: false,
                oldClrType: typeof(int),
                oldNullable: true);

            migrationBuilder.AddForeignKey(
                name: "FK_ProProfiles_PreferredPricing_PreferredPricingID",
                table: "ProProfiles",
                column: "PreferredPricingID",
                principalTable: "PreferredPricing",
                principalColumn: "PreferredPricingID",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
