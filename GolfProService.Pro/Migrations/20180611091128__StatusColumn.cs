﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace GolfProService.Pro.Migrations
{
    public partial class _StatusColumn : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "Status",
                table: "ProProfiles",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Status",
                table: "NotablePlayersCoached",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Status",
                table: "Expertises",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Status",
                table: "DeviceInfos",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Status",
                table: "CurrencyInfo",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Status",
                table: "AgeLevels",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Status",
                table: "Accolade",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Status",
                table: "ProProfiles");

            migrationBuilder.DropColumn(
                name: "Status",
                table: "NotablePlayersCoached");

            migrationBuilder.DropColumn(
                name: "Status",
                table: "Expertises");

            migrationBuilder.DropColumn(
                name: "Status",
                table: "DeviceInfos");

            migrationBuilder.DropColumn(
                name: "Status",
                table: "CurrencyInfo");

            migrationBuilder.DropColumn(
                name: "Status",
                table: "AgeLevels");

            migrationBuilder.DropColumn(
                name: "Status",
                table: "Accolade");
        }
    }
}
