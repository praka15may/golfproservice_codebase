﻿using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace GolfProService.Pro.Migrations
{
    public partial class _PreferredPriceChange : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_PreferredPricing_PricingCategory_PricingCategoryID",
                table: "PreferredPricing");

            migrationBuilder.DropTable(
                name: "PricingCategory");

            migrationBuilder.DropIndex(
                name: "IX_PreferredPricing_PricingCategoryID",
                table: "PreferredPricing");

            migrationBuilder.DropColumn(
                name: "PricingCategoryID",
                table: "PreferredPricing");

            migrationBuilder.RenameColumn(
                name: "Price",
                table: "PreferredPricing",
                newName: "Package");

            migrationBuilder.AddColumn<double>(
                name: "Group",
                table: "PreferredPricing",
                nullable: false,
                defaultValue: 0.0);

            migrationBuilder.AddColumn<double>(
                name: "Individual",
                table: "PreferredPricing",
                nullable: false,
                defaultValue: 0.0);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Group",
                table: "PreferredPricing");

            migrationBuilder.DropColumn(
                name: "Individual",
                table: "PreferredPricing");

            migrationBuilder.RenameColumn(
                name: "Package",
                table: "PreferredPricing",
                newName: "Price");

            migrationBuilder.AddColumn<int>(
                name: "PricingCategoryID",
                table: "PreferredPricing",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.CreateTable(
                name: "PricingCategory",
                columns: table => new
                {
                    PricingCategoryID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Description = table.Column<string>(nullable: true),
                    Name = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PricingCategory", x => x.PricingCategoryID);
                });

            migrationBuilder.CreateIndex(
                name: "IX_PreferredPricing_PricingCategoryID",
                table: "PreferredPricing",
                column: "PricingCategoryID");

            migrationBuilder.AddForeignKey(
                name: "FK_PreferredPricing_PricingCategory_PricingCategoryID",
                table: "PreferredPricing",
                column: "PricingCategoryID",
                principalTable: "PricingCategory",
                principalColumn: "PricingCategoryID",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
