﻿using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace GolfProService.Pro.Migrations
{
    public partial class Initial : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "AgeLevels",
                columns: table => new
                {
                    AgeLevelID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Description = table.Column<string>(nullable: true),
                    Level = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AgeLevels", x => x.AgeLevelID);
                });

            migrationBuilder.CreateTable(
                name: "CurrencyInfo",
                columns: table => new
                {
                    CurrencyInfoID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Country = table.Column<string>(nullable: true),
                    Currency = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CurrencyInfo", x => x.CurrencyInfoID);
                });

            migrationBuilder.CreateTable(
                name: "Expertises",
                columns: table => new
                {
                    ExpertiseID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Description = table.Column<string>(nullable: true),
                    Name = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Expertises", x => x.ExpertiseID);
                });

            migrationBuilder.CreateTable(
                name: "PricingCategory",
                columns: table => new
                {
                    PricingCategoryID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Description = table.Column<string>(nullable: true),
                    Name = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PricingCategory", x => x.PricingCategoryID);
                });

            migrationBuilder.CreateTable(
                name: "ProProfiles",
                columns: table => new
                {
                    ProProfileID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    CourseId = table.Column<int>(nullable: false),
                    GolfProUserID = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ProProfiles", x => x.ProProfileID);
                });

            migrationBuilder.CreateTable(
                name: "Accolade",
                columns: table => new
                {
                    AccoladeID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Description = table.Column<string>(nullable: true),
                    Name = table.Column<string>(nullable: true),
                    ProProfileID = table.Column<int>(nullable: true),
                    Year = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Accolade", x => x.AccoladeID);
                    table.ForeignKey(
                        name: "FK_Accolade_ProProfiles_ProProfileID",
                        column: x => x.ProProfileID,
                        principalTable: "ProProfiles",
                        principalColumn: "ProProfileID",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "BankAccountInfos",
                columns: table => new
                {
                    BankingInfoID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    AccountHolderName = table.Column<string>(nullable: true),
                    AccountNumber = table.Column<long>(nullable: false),
                    ProProfileID = table.Column<int>(nullable: true),
                    RoutingNumber = table.Column<long>(nullable: false),
                    Status = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_BankAccountInfos", x => x.BankingInfoID);
                    table.ForeignKey(
                        name: "FK_BankAccountInfos_ProProfiles_ProProfileID",
                        column: x => x.ProProfileID,
                        principalTable: "ProProfiles",
                        principalColumn: "ProProfileID",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "DeviceInfos",
                columns: table => new
                {
                    DeviceInfoID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Android_Token = table.Column<string>(nullable: true),
                    Device_Platform = table.Column<string>(nullable: true),
                    Device_Type = table.Column<string>(nullable: true),
                    IOS_Token = table.Column<string>(nullable: true),
                    ProProfileID = table.Column<int>(nullable: true),
                    UA_Token = table.Column<string>(nullable: true),
                    UUID = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_DeviceInfos", x => x.DeviceInfoID);
                    table.ForeignKey(
                        name: "FK_DeviceInfos_ProProfiles_ProProfileID",
                        column: x => x.ProProfileID,
                        principalTable: "ProProfiles",
                        principalColumn: "ProProfileID",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "NotablePlayersCoached",
                columns: table => new
                {
                    NotablePlayersCoachedID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    FirstName = table.Column<string>(nullable: true),
                    LastName = table.Column<string>(nullable: true),
                    ProProfileID = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_NotablePlayersCoached", x => x.NotablePlayersCoachedID);
                    table.ForeignKey(
                        name: "FK_NotablePlayersCoached_ProProfiles_ProProfileID",
                        column: x => x.ProProfileID,
                        principalTable: "ProProfiles",
                        principalColumn: "ProProfileID",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "PreferredAgeLevels",
                columns: table => new
                {
                    PreferredAgeLevelID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    AgeLevelId = table.Column<int>(nullable: false),
                    ProProfileID = table.Column<int>(nullable: true),
                    Status = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PreferredAgeLevels", x => x.PreferredAgeLevelID);
                    table.ForeignKey(
                        name: "FK_PreferredAgeLevels_AgeLevels_AgeLevelId",
                        column: x => x.AgeLevelId,
                        principalTable: "AgeLevels",
                        principalColumn: "AgeLevelID",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_PreferredAgeLevels_ProProfiles_ProProfileID",
                        column: x => x.ProProfileID,
                        principalTable: "ProProfiles",
                        principalColumn: "ProProfileID",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "PreferredExpertises",
                columns: table => new
                {
                    PreferredExpertiseID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    ExpertiseID = table.Column<int>(nullable: false),
                    ProProfileID = table.Column<int>(nullable: true),
                    Status = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PreferredExpertises", x => x.PreferredExpertiseID);
                    table.ForeignKey(
                        name: "FK_PreferredExpertises_Expertises_ExpertiseID",
                        column: x => x.ExpertiseID,
                        principalTable: "Expertises",
                        principalColumn: "ExpertiseID",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_PreferredExpertises_ProProfiles_ProProfileID",
                        column: x => x.ProProfileID,
                        principalTable: "ProProfiles",
                        principalColumn: "ProProfileID",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "PreferredPricing",
                columns: table => new
                {
                    PreferredPricingID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    CurrencyID = table.Column<int>(nullable: false),
                    CurrencyInfoID = table.Column<int>(nullable: false),
                    Duration = table.Column<long>(nullable: false),
                    Price = table.Column<double>(nullable: false),
                    PricingCategoryID = table.Column<int>(nullable: false),
                    ProProfileID = table.Column<int>(nullable: true),
                    Status = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PreferredPricing", x => x.PreferredPricingID);
                    table.ForeignKey(
                        name: "FK_PreferredPricing_CurrencyInfo_CurrencyID",
                        column: x => x.CurrencyID,
                        principalTable: "CurrencyInfo",
                        principalColumn: "CurrencyInfoID",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_PreferredPricing_PricingCategory_PricingCategoryID",
                        column: x => x.PricingCategoryID,
                        principalTable: "PricingCategory",
                        principalColumn: "PricingCategoryID",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_PreferredPricing_ProProfiles_ProProfileID",
                        column: x => x.ProProfileID,
                        principalTable: "ProProfiles",
                        principalColumn: "ProProfileID",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Accolade_ProProfileID",
                table: "Accolade",
                column: "ProProfileID");

            migrationBuilder.CreateIndex(
                name: "IX_BankAccountInfos_ProProfileID",
                table: "BankAccountInfos",
                column: "ProProfileID");

            migrationBuilder.CreateIndex(
                name: "IX_DeviceInfos_ProProfileID",
                table: "DeviceInfos",
                column: "ProProfileID");

            migrationBuilder.CreateIndex(
                name: "IX_NotablePlayersCoached_ProProfileID",
                table: "NotablePlayersCoached",
                column: "ProProfileID");

            migrationBuilder.CreateIndex(
                name: "IX_PreferredAgeLevels_AgeLevelId",
                table: "PreferredAgeLevels",
                column: "AgeLevelId");

            migrationBuilder.CreateIndex(
                name: "IX_PreferredAgeLevels_ProProfileID",
                table: "PreferredAgeLevels",
                column: "ProProfileID");

            migrationBuilder.CreateIndex(
                name: "IX_PreferredExpertises_ExpertiseID",
                table: "PreferredExpertises",
                column: "ExpertiseID");

            migrationBuilder.CreateIndex(
                name: "IX_PreferredExpertises_ProProfileID",
                table: "PreferredExpertises",
                column: "ProProfileID");

            migrationBuilder.CreateIndex(
                name: "IX_PreferredPricing_CurrencyID",
                table: "PreferredPricing",
                column: "CurrencyID");

            migrationBuilder.CreateIndex(
                name: "IX_PreferredPricing_PricingCategoryID",
                table: "PreferredPricing",
                column: "PricingCategoryID");

            migrationBuilder.CreateIndex(
                name: "IX_PreferredPricing_ProProfileID",
                table: "PreferredPricing",
                column: "ProProfileID");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Accolade");

            migrationBuilder.DropTable(
                name: "BankAccountInfos");

            migrationBuilder.DropTable(
                name: "DeviceInfos");

            migrationBuilder.DropTable(
                name: "NotablePlayersCoached");

            migrationBuilder.DropTable(
                name: "PreferredAgeLevels");

            migrationBuilder.DropTable(
                name: "PreferredExpertises");

            migrationBuilder.DropTable(
                name: "PreferredPricing");

            migrationBuilder.DropTable(
                name: "AgeLevels");

            migrationBuilder.DropTable(
                name: "Expertises");

            migrationBuilder.DropTable(
                name: "CurrencyInfo");

            migrationBuilder.DropTable(
                name: "PricingCategory");

            migrationBuilder.DropTable(
                name: "ProProfiles");
        }
    }
}
