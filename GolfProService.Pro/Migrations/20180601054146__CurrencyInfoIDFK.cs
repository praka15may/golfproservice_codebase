﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace GolfProService.Pro.Migrations
{
    public partial class _CurrencyInfoIDFK : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_PreferredPricing_CurrencyInfo_CurrencyID",
                table: "PreferredPricing");

            migrationBuilder.DropIndex(
                name: "IX_PreferredPricing_CurrencyID",
                table: "PreferredPricing");

            migrationBuilder.DropColumn(
                name: "CurrencyID",
                table: "PreferredPricing");

            migrationBuilder.AlterColumn<int>(
                name: "Duration",
                table: "PreferredPricing",
                nullable: false,
                oldClrType: typeof(long));

            migrationBuilder.CreateIndex(
                name: "IX_PreferredPricing_CurrencyInfoID",
                table: "PreferredPricing",
                column: "CurrencyInfoID");

            migrationBuilder.AddForeignKey(
                name: "FK_PreferredPricing_CurrencyInfo_CurrencyInfoID",
                table: "PreferredPricing",
                column: "CurrencyInfoID",
                principalTable: "CurrencyInfo",
                principalColumn: "CurrencyInfoID",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_PreferredPricing_CurrencyInfo_CurrencyInfoID",
                table: "PreferredPricing");

            migrationBuilder.DropIndex(
                name: "IX_PreferredPricing_CurrencyInfoID",
                table: "PreferredPricing");

            migrationBuilder.AlterColumn<long>(
                name: "Duration",
                table: "PreferredPricing",
                nullable: false,
                oldClrType: typeof(int));

            migrationBuilder.AddColumn<int>(
                name: "CurrencyID",
                table: "PreferredPricing",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.CreateIndex(
                name: "IX_PreferredPricing_CurrencyID",
                table: "PreferredPricing",
                column: "CurrencyID");

            migrationBuilder.AddForeignKey(
                name: "FK_PreferredPricing_CurrencyInfo_CurrencyID",
                table: "PreferredPricing",
                column: "CurrencyID",
                principalTable: "CurrencyInfo",
                principalColumn: "CurrencyInfoID",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
