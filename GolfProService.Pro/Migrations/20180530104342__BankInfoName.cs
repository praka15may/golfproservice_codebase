﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace GolfProService.Pro.Migrations
{
    public partial class _BankInfoName : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "AccountHolderName",
                table: "BankAccountInfos",
                newName: "AccountHolderLastName");

            migrationBuilder.AddColumn<string>(
                name: "AccountHolderFirstName",
                table: "BankAccountInfos",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "AccountHolderFirstName",
                table: "BankAccountInfos");

            migrationBuilder.RenameColumn(
                name: "AccountHolderLastName",
                table: "BankAccountInfos",
                newName: "AccountHolderName");
        }
    }
}
