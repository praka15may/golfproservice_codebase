﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace GolfProService.Pro.Migrations
{
    public partial class _AccountHolderName : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "AccountHolderName",
                table: "BankAccountInfos",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "AccountType",
                table: "BankAccountInfos",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "AccountHolderName",
                table: "BankAccountInfos");

            migrationBuilder.DropColumn(
                name: "AccountType",
                table: "BankAccountInfos");
        }
    }
}
