﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace GolfProService.Pro.Migrations
{
    public partial class _ProProfilePrefferedPricingChange : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_PreferredPricing_ProProfiles_ProProfileID",
                table: "PreferredPricing");

            migrationBuilder.DropIndex(
                name: "IX_PreferredPricing_ProProfileID",
                table: "PreferredPricing");

            migrationBuilder.DropColumn(
                name: "ProProfileID",
                table: "PreferredPricing");

            migrationBuilder.AddColumn<int>(
                name: "PreferredPricingID",
                table: "ProProfiles",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.CreateIndex(
                name: "IX_ProProfiles_PreferredPricingID",
                table: "ProProfiles",
                column: "PreferredPricingID");

            migrationBuilder.AddForeignKey(
                name: "FK_ProProfiles_PreferredPricing_PreferredPricingID",
                table: "ProProfiles",
                column: "PreferredPricingID",
                principalTable: "PreferredPricing",
                principalColumn: "PreferredPricingID",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_ProProfiles_PreferredPricing_PreferredPricingID",
                table: "ProProfiles");

            migrationBuilder.DropIndex(
                name: "IX_ProProfiles_PreferredPricingID",
                table: "ProProfiles");

            migrationBuilder.DropColumn(
                name: "PreferredPricingID",
                table: "ProProfiles");

            migrationBuilder.AddColumn<int>(
                name: "ProProfileID",
                table: "PreferredPricing",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_PreferredPricing_ProProfileID",
                table: "PreferredPricing",
                column: "ProProfileID");

            migrationBuilder.AddForeignKey(
                name: "FK_PreferredPricing_ProProfiles_ProProfileID",
                table: "PreferredPricing",
                column: "ProProfileID",
                principalTable: "ProProfiles",
                principalColumn: "ProProfileID",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
