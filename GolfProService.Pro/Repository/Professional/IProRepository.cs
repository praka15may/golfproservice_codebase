﻿using GolfProService.Pro.Models.DTO.ProProfile;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GolfProService.Pro.Repository.Professional
{
    public interface IProRepository
    {
        List<ProQuickProfilBasicInfoDTO> GetProQuickProfileBasicInfo(List<ProQuickProfilBasicInfoDTO> proProfiles);
        ProfileDTO GetProfileInfoByUserID(UserInfoDTO userInfo);
        List<ExpertiseDTO> GetAllExpertise();
        List<AgeLevelDTO> GetAllAgeLevels();
        Task<ProfileDTO> UpdateProfileInfoByUserID(ProfilePersonalInfoDTO userInfo, string userToken);
        BankInfoDTO AddOrUpdateBankInfo(int userID, BankInfoDTO bankInfo);
        PreferredPricingDTO AddOrUpdatPreferredPricing(int userID, PreferredPricingDTO pricingRate);
        PreferredTeachingStyleDTO AddOrUpdateTeachingStyle(int userID, PreferredTeachingStyleDTO preferredTeachingStyleDTO);
        ProQuickProfileDTO GetProQuickProfileInfo(int userID);
    }
}
