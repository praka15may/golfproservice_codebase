﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using GolfProService.Pro.Infrastructure.Exceptions;
using GolfProService.Pro.Models;
using GolfProService.Pro.Models.DTO.ProProfile;
using GolfProService.Pro.Models.Enum;
using GolfProService.Pro.Services.Professional;
using Microsoft.EntityFrameworkCore;

namespace GolfProService.Pro.Repository.Professional
{
    internal class ProRepository : IProRepository
    {
        private ProfessionalContext ProfessionalContext = null;
        private IProService ProService = null;
        private readonly IMapper Mapper = null;

        public ProRepository(ProfessionalContext professionalContext, IProService proService, IMapper mapper)
        {
            ProfessionalContext = professionalContext;
            ProService = proService;
            Mapper = mapper;
        }


        public List<ProQuickProfilBasicInfoDTO> GetProQuickProfileBasicInfo(List<ProQuickProfilBasicInfoDTO> proProfiles)
        {
            try
            {
                if(proProfiles.Count > 0)
                {
                    proProfiles = CreateProQuickBasicInfoObject(proProfiles);
                }
                else
                {
                    throw new NoProFoundException();
                }
            }
            catch(Exception ex)
            {
                throw ex;
            }
            return proProfiles;
        }

        /// <summary>
        /// Get Profile Information from Pro APi service DB
        /// </summary>
        /// <param name="userInfo"></param>
        /// <returns></returns>
        public ProfileDTO GetProfileInfoByUserID(UserInfoDTO userInfo)
        {
            ProfileDTO profileInfo = null;
            try
            {
                ProProfile basicInfo = ProfessionalContext.ProProfiles.Where(u => u.GolfProUserID == userInfo.UserID)
                    .Include(pr => pr.PricePreference).ThenInclude(e => e.CurrencyInfo)
                    .Include(nt => nt.PlayersCoached)
                    .Include(bnk => bnk.BankingInfos)
                    .Include(exp => exp.PreferredExpertises).ThenInclude(e => e.Expertise)
                    .Include(ag => ag.AgeLevels).ThenInclude(e => e.AgeLevel)
                    .Include(dv => dv.DeviceInfos)
                    .Include(ac => ac.Accolades).FirstOrDefault();
                if(basicInfo != null)
                {
                    basicInfo.Accolades = basicInfo.Accolades.Where(s => s.Status != StatusType.Inactive.ToString()).ToList();
                    basicInfo.AgeLevels = basicInfo.AgeLevels.Where(s => s.Status != StatusType.Inactive.ToString()).ToList();
                    basicInfo.BankingInfos = basicInfo.BankingInfos.Where(s => s.Status != StatusType.Inactive.ToString()).ToList();
                    basicInfo.DeviceInfos = basicInfo.DeviceInfos.Where(s => s.Status != StatusType.Inactive.ToString()).ToList();
                    basicInfo.PlayersCoached = basicInfo.PlayersCoached.Where(s => s.Status != StatusType.Inactive.ToString()).ToList();
                    basicInfo.PreferredExpertises = basicInfo.PreferredExpertises.Where(s => s.Status != StatusType.Inactive.ToString()).ToList();                    
                }
                profileInfo = MapProfileInfoObject(basicInfo, userInfo);

            }
            catch(Exception ex)
            {
                throw ex;
            }

            return profileInfo;
        }

        /// <summary>
        /// Get Pro profile info for Quick view
        /// </summary>
        /// <param name="userID"></param>
        /// <returns></returns>
        public ProQuickProfileDTO GetProQuickProfileInfo(int userID)
        {
            ProQuickProfileDTO proQuickProfile = null;
            ProProfile profileInfo = null;
            try
            {
                profileInfo = ProfessionalContext.ProProfiles.Where(u => u.GolfProUserID == userID)
                    .Include(pr => pr.PricePreference).ThenInclude(e => e.CurrencyInfo)
                    .Include(nt => nt.PlayersCoached)
                    .Include(bnk => bnk.BankingInfos)
                    .Include(exp => exp.PreferredExpertises).ThenInclude(e => e.Expertise)
                    .Include(ag => ag.AgeLevels).ThenInclude(a => a.AgeLevel)
                    .Include(dv => dv.DeviceInfos)
                    .Include(ac => ac.Accolades)
                    .FirstOrDefault();
                if (profileInfo != null)
                {
                    profileInfo.Accolades = profileInfo.Accolades.Where(s => s.Status != StatusType.Inactive.ToString()).ToList();
                    profileInfo.AgeLevels = profileInfo.AgeLevels.Where(s => s.Status != StatusType.Inactive.ToString()).ToList();
                    profileInfo.BankingInfos = profileInfo.BankingInfos.Where(s => s.Status != StatusType.Inactive.ToString()).ToList();
                    profileInfo.DeviceInfos = profileInfo.DeviceInfos.Where(s => s.Status != StatusType.Inactive.ToString()).ToList();
                    profileInfo.PlayersCoached = profileInfo.PlayersCoached.Where(s => s.Status != StatusType.Inactive.ToString()).ToList();
                    profileInfo.PreferredExpertises = profileInfo.PreferredExpertises.Where(s => s.Status != StatusType.Inactive.ToString()).ToList();                    
                }
                proQuickProfile = CreateQuickProfileObject(profileInfo);
            }
            catch(Exception ex)
            {
                throw ex;
            }

            return proQuickProfile;
        }

        /// <summary>
        /// Get all Expertise master data
        /// </summary>
        /// <returns></returns>
        public List<ExpertiseDTO> GetAllExpertise()
        {
            List<ExpertiseDTO> expertiseDTOList = null;
            List<Expertise> expertiseList = null;
            try
            {
                expertiseList = ProfessionalContext.Expertises.Where(s => s.Status != StatusType.Inactive.ToString()).ToList();
                expertiseDTOList = Mapper.Map<List<Expertise>, List<ExpertiseDTO>>(expertiseList);
            }
            catch(Exception ex)
            {
                throw ex;
            }

            return expertiseDTOList;
        }

        /// <summary>
        /// Get all Agel level masterdata
        /// </summary>
        /// <returns></returns>
        public List<AgeLevelDTO> GetAllAgeLevels()
        {
            List<AgeLevelDTO> ageLevelDTOList = null;
            List<AgeLevel> ageLevelList = null;
            try
            {
                ageLevelList = ProfessionalContext.AgeLevels.Where(s => s.Status != StatusType.Inactive.ToString()).ToList();
                ageLevelDTOList = Mapper.Map<List<AgeLevel>, List<AgeLevelDTO>>(ageLevelList);
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return ageLevelDTOList;
        }

        /// <summary>
        /// Update Pro profile info in marketplace api and add/update profile info in Pro service db
        /// </summary>
        /// <param name="userInfo"></param>
        /// <param name="userToken"></param>
        /// <returns></returns>
        public async Task<ProfileDTO> UpdateProfileInfoByUserID(ProfilePersonalInfoDTO userInfo, string userToken)
        {
            UserInfoDTO userprofileInfo = null;
            ProfileDTO profile = null;
            bool IsProfileUpdated = false;
            try
            {
                userprofileInfo = MapPersonalInfoToUserProfile(userInfo);
                IsProfileUpdated = await ProfileUpdateAtSGM(userprofileInfo, userToken);
                if (IsProfileUpdated)
                {
                    ProfileUpdateAtGolfPro(userprofileInfo.UserID, userInfo);
                    profile = GetProfileInfoByUserID(userprofileInfo);
                }
                else
                {
                    throw new Exception("Something went wrong! Please try after some time");
                }
                    
            }
            catch(Exception ex)
            {
                throw ex;
            }

            return profile;
        }

        /// <summary>
        /// Pro profile personal information update
        /// </summary>
        /// <param name="userID"></param>
        /// <param name="bankInfo"></param>
        /// <returns></returns>
        public BankInfoDTO AddOrUpdateBankInfo(int userID, BankInfoDTO bankInfo)
        {
            BankAccountInfo bankAccount = null;
            List<BankAccountInfo> bankAccountList = null;
            try
            {
                ProProfile basicInfo = ProfessionalContext.ProProfiles.Where(u => u.GolfProUserID == userID)                    
                    .Include(bnk => bnk.BankingInfos).FirstOrDefault();
                if(basicInfo != null)
                {
                    UpdateBankInfo(bankInfo, ref bankAccount, ref bankAccountList, basicInfo);
                    ProfessionalContext.Update(basicInfo);
                }
                else
                {
                    basicInfo = CreateProProfile(userID);
                    basicInfo.BankingInfos = new List<BankAccountInfo>();
                    UpdateBankInfo(bankInfo, ref bankAccount, ref bankAccountList, basicInfo);
                    ProfessionalContext.Add(basicInfo);
                } 
                ProfessionalContext.SaveChanges();
                bankInfo.BankingInfoID = basicInfo.BankingInfos.FirstOrDefault().BankingInfoID;
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return bankInfo;
        }       

        /// <summary>
        /// Add/Update pro pricing information
        /// </summary>
        /// <param name="userID"></param>
        /// <param name=""></param>
        /// <returns></returns>
        public PreferredPricingDTO AddOrUpdatPreferredPricing(int userID, PreferredPricingDTO pricingRate)
        {
            PreferredPricing preferredPricing = null;
            try
            {
                ProProfile basicInfo = ProfessionalContext.ProProfiles.Where(u => u.GolfProUserID == userID)
                    .Include(pr => pr.PricePreference).FirstOrDefault();
                if (basicInfo != null)
                {
                    UpdatePreferredPricing(pricingRate, preferredPricing, basicInfo);
                }
                else
                {
                    basicInfo = CreateProProfile(userID);
                    basicInfo.PricePreference = new PreferredPricing();
                    UpdatePreferredPricing(pricingRate, preferredPricing, basicInfo);
                }
                ProfessionalContext.Update(basicInfo);
                ProfessionalContext.SaveChanges();
                pricingRate.PreferredPricingID = basicInfo.PricePreference.PreferredPricingID;
            }
            catch(Exception ex)
            {
                throw ex;
            }

            return pricingRate;
        }
        

        /// <summary>
        /// Add or Update pro teaching style information
        /// </summary>
        /// <param name="userID"></param>
        /// <param name="preferredTeachingStyleDTO"></param>
        /// <returns></returns>
        public PreferredTeachingStyleDTO AddOrUpdateTeachingStyle(int userID, PreferredTeachingStyleDTO preferredTeachingStyleDTO)
        {
            try
            {
                ProProfile basicInfo = ProfessionalContext.ProProfiles.Where(u => u.GolfProUserID == userID)
                    .Include(pr => pr.PricePreference)
                    .Include(nt => nt.PlayersCoached)
                    .Include(bnk => bnk.BankingInfos)
                    .Include(exp => exp.PreferredExpertises)
                    .Include(ag => ag.AgeLevels)
                    .Include(dv => dv.DeviceInfos)
                    .Include(ac => ac.Accolades).FirstOrDefault();
                if (basicInfo != null)
                {
                    AddOrUpdatePreferredExpertise(preferredTeachingStyleDTO, basicInfo);
                    AddOrUpdatePreferredAgeLevels(preferredTeachingStyleDTO, basicInfo);
                    AddOrUpdateAccolades(preferredTeachingStyleDTO, basicInfo);
                    AddOrUpdateNotablePlayersCoached(preferredTeachingStyleDTO, basicInfo);
                    basicInfo.PGACertified = preferredTeachingStyleDTO.PGACertified;
                }
                else
                {
                    basicInfo = CreateProProfile(userID);
                    basicInfo.PreferredExpertises = new List<PreferredExpertise>();
                    AddOrUpdatePreferredExpertise(preferredTeachingStyleDTO, basicInfo);
                    basicInfo.AgeLevels = new List<PreferredAgeLevel>();
                    AddOrUpdatePreferredAgeLevels(preferredTeachingStyleDTO, basicInfo);
                    basicInfo.Accolades = new List<Accolade>();
                    AddOrUpdateAccolades(preferredTeachingStyleDTO, basicInfo);
                    basicInfo.PlayersCoached = new List<NotablePlayersCoached>();
                    AddOrUpdateNotablePlayersCoached(preferredTeachingStyleDTO, basicInfo);
                    basicInfo.PGACertified = preferredTeachingStyleDTO.PGACertified;                    
                }
                ProfessionalContext.Update(basicInfo);
                ProfessionalContext.SaveChanges();
                preferredTeachingStyleDTO = MapTeachingStyleDataWithDTO(basicInfo);
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return preferredTeachingStyleDTO;
        }

        #region Private Methods

        private PreferredTeachingStyleDTO MapTeachingStyleDataWithDTO(ProProfile basicInfo)
        {
            PreferredTeachingStyleDTO preferredTeachingStyleDTO = null;
            try
            {
                basicInfo.Accolades = basicInfo.Accolades.Where(s => s.Status != StatusType.Inactive.ToString()).ToList();
                basicInfo.AgeLevels = basicInfo.AgeLevels.Where(s => s.Status != StatusType.Inactive.ToString()).ToList();
                basicInfo.PlayersCoached = basicInfo.PlayersCoached.Where(s => s.Status != StatusType.Inactive.ToString()).ToList();
                basicInfo.PreferredExpertises = basicInfo.PreferredExpertises.Where(s => s.Status != StatusType.Inactive.ToString()).ToList();

                preferredTeachingStyleDTO = new PreferredTeachingStyleDTO();
                preferredTeachingStyleDTO.Accolades = Mapper.Map<List<Accolade>, List<AccoladeDTO>>(basicInfo.Accolades);
                preferredTeachingStyleDTO.AgeLevels = Mapper.Map<List<PreferredAgeLevel>, List<PreferredAgeLevelDTO>>(basicInfo.AgeLevels);
                preferredTeachingStyleDTO.ExpertiseLevels = Mapper.Map<List<PreferredExpertise>, List<PrefferedExpertiseDTO>>(basicInfo.PreferredExpertises);
                preferredTeachingStyleDTO.NotablePlayersCoached = Mapper.Map<List<NotablePlayersCoached>, List<NotablePlayersCoachedDTO>>(basicInfo.PlayersCoached);
                preferredTeachingStyleDTO.PGACertified = basicInfo.PGACertified;
            }
            catch(Exception ex)
            {
                throw ex;
            }
            return preferredTeachingStyleDTO;
        }

        /// <summary>
        /// Update preferred pricing information
        /// </summary>
        /// <param name="pricingRate"></param>
        /// <param name="preferredPricing"></param>
        /// <param name="basicInfo"></param>
        /// <returns></returns>
        private void UpdatePreferredPricing(PreferredPricingDTO pricingRate, PreferredPricing preferredPricing, ProProfile basicInfo)
        {
            try
            {
                if (basicInfo.PreferredPricingID == null)
                {
                    basicInfo.PricePreference = Mapper.Map<PreferredPricingDTO, PreferredPricing>(pricingRate);
                }
                else
                {
                    preferredPricing = Mapper.Map<PreferredPricingDTO, PreferredPricing>(pricingRate, preferredPricing);
                    basicInfo.PricePreference = Mapper.Map<PreferredPricing, PreferredPricing>(preferredPricing, basicInfo.PricePreference);

                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Craete record in ProProfile table with SGM user id
        /// </summary>
        /// <param name="userID"></param>
        /// <returns></returns>
        private ProProfile CreateProProfile(int userID)
        {
            ProProfile basicInfo = null;
            try
            {
                basicInfo = new ProProfile();
                basicInfo.CourseId = 0;
                basicInfo.GolfProUserID = userID;
                basicInfo.PGACertified = false;
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return basicInfo;
        }

        /// <summary>
        /// Update bank info for pro profile
        /// </summary>
        /// <param name="bankInfo"></param>
        /// <param name="bankAccount"></param>
        /// <param name="bankAccountList"></param>
        /// <param name="basicInfo"></param>
        private void UpdateBankInfo(BankInfoDTO bankInfo, ref BankAccountInfo bankAccount, ref List<BankAccountInfo> bankAccountList, ProProfile basicInfo)
        {
            try
            {
                if (bankInfo.BankingInfoID == 0)
                {
                    bankAccountList = new List<BankAccountInfo>();
                    bankInfo.Status = "Active";
                    bankAccount = Mapper.Map<BankInfoDTO, BankAccountInfo>(bankInfo);
                    bankAccountList.Add(bankAccount);
                    basicInfo.BankingInfos = Mapper.Map<List<BankAccountInfo>, List<BankAccountInfo>>(bankAccountList);
                }
                else
                {
                    bankAccount = Mapper.Map<BankInfoDTO, BankAccountInfo>(bankInfo, bankAccount);
                    BankAccountInfo bankAccountOld = ProfessionalContext.BankAccountInfos.Where(b => b.BankingInfoID == bankInfo.BankingInfoID).FirstOrDefault();
                    bankAccountOld = Mapper.Map<BankAccountInfo, BankAccountInfo>(bankAccount, bankAccountOld);
                    ProfessionalContext.Update(bankAccountOld);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Map Pro service profile data and Market place profile data 
        /// </summary>
        /// <param name="basicInfo"></param>
        /// <param name="userInfo"></param>
        /// <returns></returns>
        private ProfileDTO MapProfileInfoObject(ProProfile basicInfo, UserInfoDTO userInfo)
        {
            ProfileDTO profile = null;
            try
            {
                profile = new ProfileDTO();
                profile.ProfileInfo = new ProInfoDTO();
                profile.ProfileInfo.id = userInfo.UserID;
                profile.ProfileInfo.PersonalInfo = new PersonalInfoDTO();
                profile.ProfileInfo.PersonalInfo.Email = userInfo.Email;
                profile.ProfileInfo.PersonalInfo.FirstName = userInfo.FirstName;
                profile.ProfileInfo.PersonalInfo.LastName = userInfo.LastName;
                profile.ProfileInfo.PersonalInfo.Phone = userInfo.Phone;
                profile.ProfileInfo.Address = new AddressDTO();
                profile.ProfileInfo.Address.Address1 = userInfo.Address;
                profile.ProfileInfo.Address.City = userInfo.City;
                profile.ProfileInfo.Address.State = userInfo.State;
                profile.ProfileInfo.Address.Country = userInfo.Country;
                profile.ProfileInfo.Address.Zipcode = userInfo.Zipcode;
                profile.ProfileInfo.DateOfBirth = userInfo.DateOfBirth;
                profile.ProfileInfo.Gender = userInfo.Gender;
                profile.ProfileInfo.AvatarUrl = userInfo.NewImageUrl;
                if (basicInfo != null)
                {
                    profile.ProfileInfo.ProDescription = basicInfo.ProDescription;
                    profile.ProfileInfo.PGACertified = basicInfo.PGACertified;
                    profile.ProfileInfo.CourseID = basicInfo.CourseId;
                    profile.ProfileInfo.TeachingStyle = new TeachingStyleDTO();
                    profile.ProfileInfo.TeachingStyle.AgeLevels = Mapper.Map<List<PreferredAgeLevel>, List<PreferredAgeLevelDTO>>(basicInfo.AgeLevels);
                    profile.ProfileInfo.TeachingStyle.ExpertiseLevels = Mapper.Map<List<PreferredExpertise>, List<PrefferedExpertiseDTO>>(basicInfo.PreferredExpertises);
                    profile.ProfileInfo.Accolades = Mapper.Map<List<Accolade>, List<AccoladeDTO>>(basicInfo.Accolades);
                    profile.ProfileInfo.NotablePlayersCoached = Mapper.Map<List<NotablePlayersCoached>, List<NotablePlayersCoachedDTO>>(basicInfo.PlayersCoached);                    
                    profile.ProfileInfo.BankingInfos = Mapper.Map<List<BankAccountInfo>, List<BankInfoDTO>>(basicInfo.BankingInfos);
                    profile.ProfileInfo.PreferredPricing = Mapper.Map<PreferredPricing, PreferredPricingDTO>(basicInfo.PricePreference);
                }
                
            }
            catch(Exception ex)
            {
                throw ex;
            }

            return profile;
        }      

        /// <summary>
        /// Map Personal profile object to marketplace profile object
        /// </summary>
        /// <param name="userInfo"></param>
        /// <returns></returns>
        private UserInfoDTO MapPersonalInfoToUserProfile(ProfilePersonalInfoDTO userInfo)
        {
            UserInfoDTO userprofileInfo = null;
            try
            {
                userprofileInfo = new UserInfoDTO();
                userprofileInfo.UserID = userInfo.id;
                userprofileInfo.DateOfBirth = userInfo.DateOfBirth;
                userprofileInfo.Gender = userInfo.Gender;
                userprofileInfo.Base64Image = userInfo.Base64Image;                


                if (userInfo.Address != null)
                {                    
                    userprofileInfo.Address = userInfo.Address.Address1;
                    userprofileInfo.City = userInfo.Address.City;
                    userprofileInfo.Country = userInfo.Address.Country;
                    userprofileInfo.State = userInfo.Address.State;
                    userprofileInfo.Zipcode = userInfo.Address.Zipcode;
                }
                if(userInfo.PersonalInfo != null)
                {                    
                    userprofileInfo.Email = userInfo.PersonalInfo.Email;
                    userprofileInfo.FirstName = userInfo.PersonalInfo.FirstName;                    
                    userprofileInfo.LastName = userInfo.PersonalInfo.LastName;
                    userprofileInfo.Phone = userInfo.PersonalInfo.Phone;
                }
                
                
            }
            catch(Exception ex)
            {
                throw ex;
            }

            return userprofileInfo;
        }

        /// <summary>
        /// Marketplace api implementation to update profile personal information
        /// </summary>
        /// <param name="userProfileInfo"></param>
        /// <param name="userToken"></param>
        /// <returns></returns>
        private async Task<bool> ProfileUpdateAtSGM(UserInfoDTO userProfileInfo, string userToken)
        {
            bool IsProfileUpdated = false;
            try
            {
                IsProfileUpdated = await ProService.UpdateProfileInfoByToken(userProfileInfo, userToken);
            }
            catch(Exception ex)
            {
                throw ex;
            }

            return IsProfileUpdated;
        }

        /// <summary>
        /// Update the profile information at pro service db
        /// </summary>
        /// <param name="userID"></param>
        /// <param name="userInfo"></param>
        private void ProfileUpdateAtGolfPro(int userID, ProfilePersonalInfoDTO userInfo)
        {
            try
            {
                ProProfile basicInfo = ProfessionalContext.ProProfiles.FirstOrDefault(s => s.GolfProUserID == userID);
                if(basicInfo != null)
                {
                    basicInfo.CourseId = userInfo.CourseID;
                    basicInfo.GolfProUserID = userInfo.id;
                    basicInfo.PGACertified = userInfo.PGACertified;
                    basicInfo.ProDescription = userInfo.ProDescription;
                    ProfessionalContext.Update(basicInfo);
                }
                else
                {
                    basicInfo = new ProProfile();
                    basicInfo.CourseId = userInfo.CourseID;
                    basicInfo.GolfProUserID = userInfo.id;
                    basicInfo.PGACertified = userInfo.PGACertified;
                    basicInfo.ProDescription = userInfo.ProDescription;
                    ProfessionalContext.Add(basicInfo);
                }
                
                ProfessionalContext.SaveChanges();
            }
            catch(Exception ex)
            {
                throw ex;
            }
        }

        private void AddOrUpdatePreferredExpertise(PreferredTeachingStyleDTO preferredTeachingStyleDTO, ProProfile basicInfo)
        {
            List<PreferredExpertise> ExpertiseLevels = null;
            if (basicInfo.PreferredExpertises.Count == 0)
            {
                basicInfo.PreferredExpertises = new List<PreferredExpertise>();
                ExpertiseLevels = Mapper.Map<List<PrefferedExpertiseDTO>, List<PreferredExpertise>>(preferredTeachingStyleDTO.ExpertiseLevels, ExpertiseLevels);
                basicInfo.PreferredExpertises = ExpertiseLevels;
            }
            else
            {
                ExpertiseLevels = Mapper.Map<List<PrefferedExpertiseDTO>, List<PreferredExpertise>>(preferredTeachingStyleDTO.ExpertiseLevels, ExpertiseLevels);
                foreach (var expertiseLevel in ExpertiseLevels)
                {
                    if(expertiseLevel.PreferredExpertiseID != 0)
                    {
                        var expertise = basicInfo.PreferredExpertises.FirstOrDefault(p => p.PreferredExpertiseID == expertiseLevel.PreferredExpertiseID);
                        expertise = Mapper.Map<PreferredExpertise, PreferredExpertise>(expertiseLevel, expertise);
                    }
                    else
                    {
                        basicInfo.PreferredExpertises.Add(expertiseLevel);
                    }
                }                
            }
        }

        private void AddOrUpdatePreferredAgeLevels(PreferredTeachingStyleDTO preferredTeachingStyleDTO, ProProfile basicInfo)
        {
            List<PreferredAgeLevel> agelevels = null;
            if (basicInfo.AgeLevels.Count == 0)
            {
                basicInfo.AgeLevels = new List<PreferredAgeLevel>();
                agelevels = Mapper.Map<List<PreferredAgeLevelDTO>, List<PreferredAgeLevel>>(preferredTeachingStyleDTO.AgeLevels, agelevels);
                basicInfo.AgeLevels = agelevels;
            }
            else
            {
                agelevels = Mapper.Map<List<PreferredAgeLevelDTO>, List<PreferredAgeLevel>>(preferredTeachingStyleDTO.AgeLevels, agelevels);
                foreach (var agelevel in agelevels)
                {
                    if(agelevel.PreferredAgeLevelID != 0)
                    {
                        var level = basicInfo.AgeLevels.FirstOrDefault(a => a.PreferredAgeLevelID == agelevel.PreferredAgeLevelID);
                        level = Mapper.Map<PreferredAgeLevel, PreferredAgeLevel>(agelevel, level);
                    }
                    else
                    {
                        basicInfo.AgeLevels.Add(agelevel);
                    }
                }
            }
        }

        private void AddOrUpdateAccolades(PreferredTeachingStyleDTO preferredTeachingStyleDTO, ProProfile basicInfo)
        {
            List<Accolade> accolades = null;
            if (basicInfo.Accolades.Count == 0)
            {
                basicInfo.Accolades = new List<Accolade>();
                accolades = Mapper.Map<List<AccoladeDTO>, List<Accolade>>(preferredTeachingStyleDTO.Accolades, accolades);
                basicInfo.Accolades = accolades;
            }
            else
            {
                accolades = Mapper.Map<List<AccoladeDTO>, List<Accolade>>(preferredTeachingStyleDTO.Accolades);
                foreach (var prefferedAccolade in accolades)
                {
                    if (prefferedAccolade.AccoladeID != 0)
                    {
                        var accolade = basicInfo.Accolades.FirstOrDefault(a => a.AccoladeID == prefferedAccolade.AccoladeID);
                        accolade = Mapper.Map<Accolade, Accolade>(prefferedAccolade,accolade);
                    }
                    else
                    {
                        basicInfo.Accolades.Add(prefferedAccolade);
                    }
                }
            }
        }

        private void AddOrUpdateNotablePlayersCoached(PreferredTeachingStyleDTO preferredTeachingStyleDTO, ProProfile basicInfo)
        {
            List<NotablePlayersCoached> playersCoached = null;
            if (basicInfo.PlayersCoached.Count == 0)
            {
                basicInfo.PlayersCoached = new List<NotablePlayersCoached>();
                playersCoached = Mapper.Map<List<NotablePlayersCoachedDTO>, List<NotablePlayersCoached>>(preferredTeachingStyleDTO.NotablePlayersCoached, playersCoached);
                basicInfo.PlayersCoached = playersCoached;
            }
            else
            {
                playersCoached = Mapper.Map<List<NotablePlayersCoachedDTO>, List<NotablePlayersCoached>>(preferredTeachingStyleDTO.NotablePlayersCoached, playersCoached);
                foreach (var player in playersCoached)
                {
                    if(player.NotablePlayersCoachedID != 0)
                    {
                        var playerCoached = basicInfo.PlayersCoached.FirstOrDefault(p => p.NotablePlayersCoachedID == player.NotablePlayersCoachedID);
                        playerCoached = Mapper.Map<NotablePlayersCoached, NotablePlayersCoached>(player,playerCoached);
                    }
                    else
                    {
                        basicInfo.PlayersCoached.Add(player);
                    }
                }
            }
        }        

        /// <summary>
        /// Map the Pro profile object into pro quick profile object for UI
        /// </summary>
        /// <param name="profileInfo"></param>
        /// <returns></returns>
        private ProQuickProfileDTO CreateQuickProfileObject(ProProfile profileInfo)
        {
            ProQuickProfileDTO proQuickProfile = null;
            try
            {
                proQuickProfile = new ProQuickProfileDTO();

                if(profileInfo.PreferredExpertises.Count > 0)
                    proQuickProfile.Experience = string.Join(", ", profileInfo.PreferredExpertises.Select(x => x.Expertise.Name));

                if(profileInfo.AgeLevels.Count > 0)
                    proQuickProfile.AgeRange = string.Join(", ", profileInfo.AgeLevels.Select(x => x.AgeLevel.Level));

                if (profileInfo.Accolades.Count > 0)
                    proQuickProfile.Award = string.Join(", ", profileInfo.Accolades.Select(x => x.Name));

                if (profileInfo.PlayersCoached.Count > 0)
                    proQuickProfile.NotableplayesCoached = string.Join(", ", profileInfo.PlayersCoached.Select(x => x.FirstName + " " + x.LastName));

                if (profileInfo.PricePreference != null)
                {
                    proQuickProfile.IndividualRate = profileInfo.PricePreference.Individual;
                    proQuickProfile.GroupRate = profileInfo.PricePreference.Group;
                    proQuickProfile.PackageRate = profileInfo.PricePreference.Package;
                }

                proQuickProfile.PGACertified = profileInfo.PGACertified;
                proQuickProfile.CourseId = profileInfo.CourseId;
                proQuickProfile.ProDescription = profileInfo.ProDescription;

            }
            catch(Exception ex)
            {
                throw ex;
            }

            return proQuickProfile;
        }

        /// <summary>
        /// Get Pro Profile info from Pro service database
        /// </summary>
        /// <param name="userID"></param>
        /// <returns></returns>
        private ProProfile GetProProfileByID(int userID)
        {
            ProProfile proProfile = null;
            try
            {

                proProfile = ProfessionalContext.ProProfiles.Where(u => u.GolfProUserID == userID)
                    .Include(pr => pr.PricePreference).ThenInclude(e => e.CurrencyInfo)
                    .Include(nt => nt.PlayersCoached)
                    .Include(bnk => bnk.BankingInfos)
                    .Include(exp => exp.PreferredExpertises).ThenInclude(e => e.Expertise)
                    .Include(ag => ag.AgeLevels).ThenInclude(a => a.AgeLevel)
                    .Include(dv => dv.DeviceInfos)
                    .Include(ac => ac.Accolades)
                    .FirstOrDefault();
                if(proProfile != null)
                {
                    proProfile.Accolades = proProfile.Accolades.Where(s => s.Status != StatusType.Inactive.ToString()).ToList();
                    proProfile.AgeLevels = proProfile.AgeLevels.Where(s => s.Status != StatusType.Inactive.ToString()).ToList();
                    proProfile.BankingInfos = proProfile.BankingInfos.Where(s => s.Status != StatusType.Inactive.ToString()).ToList();
                    proProfile.DeviceInfos = proProfile.DeviceInfos.Where(s => s.Status != StatusType.Inactive.ToString()).ToList();
                    proProfile.PlayersCoached = proProfile.PlayersCoached.Where(s => s.Status != StatusType.Inactive.ToString()).ToList();
                    proProfile.PreferredExpertises = proProfile.PreferredExpertises.Where(s => s.Status != StatusType.Inactive.ToString()).ToList();
                }
                
            }
            catch(Exception ex)
            {
                throw ex;
            }

            return proProfile;
        }

        private List<ProQuickProfilBasicInfoDTO> CreateProQuickBasicInfoObject(List<ProQuickProfilBasicInfoDTO> proQuickProfiles)
        {
            ProProfile proProfile = null;
            try
            {
                foreach (var profile in proQuickProfiles)
                {
                    proProfile = GetProProfileByID(profile.UserID);
                    if(proProfile != null)
                    {
                        if(proProfile.PreferredExpertises.Count > 0)
                        {
                            profile.ExpertiseLevel = new List<string>();
                            foreach (var expertise in proProfile.PreferredExpertises)
                            {
                                if(expertise.Expertise != null)
                                    profile.ExpertiseLevel.Add(expertise.Expertise.Name);
                            }                            
                        }

                        if(proProfile.AgeLevels.Count > 0)
                        {
                            profile.AgeRange = new List<string>();
                            foreach (var ageRange in proProfile.AgeLevels)
                            {
                                if (ageRange.AgeLevel != null)
                                    profile.AgeRange.Add(ageRange.AgeLevel.Level);
                            }
                        }

                        if(proProfile.PricePreference != null)
                        {
                            profile.Price = new PriceDTO();
                            profile.Price.Group = proProfile.PricePreference.Group;
                            profile.Price.Individual = proProfile.PricePreference.Individual;
                            profile.Price.Package = proProfile.PricePreference.Package;
                        }

                        profile.CourseID = proProfile.CourseId;
                    }
                }

            }
            catch(Exception ex)
            {
                throw ex;
            }

            return proQuickProfiles;
        }

        #endregion
    }
}
