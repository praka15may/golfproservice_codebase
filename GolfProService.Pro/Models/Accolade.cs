﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace GolfProService.Pro.Models
{
    public class Accolade
    {
        [Key]
        public int AccoladeID { get; set; }
        public string Name { get; set; }
        public int Year { get; set; }
        public string Description { get; set; }
        public string Status { get; set; }
    }
}
