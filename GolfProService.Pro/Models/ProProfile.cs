﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace GolfProService.Pro.Models
{
    public class ProProfile
    {
        
        [Key]
        public int ProProfileID { get; set; }
        public int GolfProUserID { get; set; }        
        public int CourseId { get; set; }
        public bool PGACertified { get; set; }
        public string ProDescription { get; set; }
        public int? PreferredPricingID { get; set; }
        [ForeignKey("PreferredPricingID")]
        public virtual PreferredPricing PricePreference { get; set; }
        public virtual List<NotablePlayersCoached> PlayersCoached { get; set; }
        public virtual List<BankAccountInfo> BankingInfos { get; set; }
        public virtual List<PreferredExpertise> PreferredExpertises { get; set; }
        public virtual List<PreferredAgeLevel> AgeLevels { get; set; }
        public virtual List<DeviceInfo> DeviceInfos { get; set; }
        public virtual List<Accolade> Accolades { get; set; }
        public string Status { get; set; }
    }
}
