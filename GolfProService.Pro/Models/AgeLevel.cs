﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace GolfProService.Pro.Models
{
    public class AgeLevel
    {
        [Key]
        public int AgeLevelID { get; set; }
        public string Level { get; set; }
        public string Description { get; set; }
        public string Status { get; set; }
    }
}
