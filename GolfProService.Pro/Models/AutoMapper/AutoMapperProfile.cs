﻿using AutoMapper;
using GolfProService.Pro.Models.DTO.ProProfile;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GolfProService.Pro.Models.AutoMapper
{
    public class AutoMapperProfile : Profile
    {
        public AutoMapperProfile()
        {
            CreateMap<BankAccountInfo, BankInfoDTO>();
            CreateMap<BankInfoDTO, BankAccountInfo>();
            CreateMap<BankAccountInfo, BankAccountInfo>();
            CreateMap<PreferredPricing, PreferredPricingDTO>();
            CreateMap<PreferredPricingDTO, PreferredPricing>().ForMember(x => x.CurrencyInfo, opt => opt.Ignore());
            CreateMap<PreferredPricing, PreferredPricing>().ForMember(x => x.CurrencyInfo, opt => opt.Ignore());
            CreateMap<Accolade, AccoladeDTO>();
            CreateMap<AccoladeDTO, Accolade>();
            CreateMap<Accolade, Accolade>();
            CreateMap<NotablePlayersCoached, NotablePlayersCoachedDTO>();
            CreateMap<NotablePlayersCoachedDTO, NotablePlayersCoached>();
            CreateMap<NotablePlayersCoached, NotablePlayersCoached>();
            CreateMap<PreferredAgeLevel, PreferredAgeLevelDTO>();
            CreateMap<PreferredAgeLevelDTO, PreferredAgeLevel>().ForMember(x => x.AgeLevel, opt => opt.Ignore());
            CreateMap<PreferredAgeLevel, PreferredAgeLevel>().ForMember(x => x.AgeLevel, opt => opt.Ignore());
            CreateMap<Expertise, ExpertiseDTO>();
            CreateMap<AgeLevel, AgeLevelDTO>();
            CreateMap<PrefferedExpertiseDTO, PreferredExpertise>().ForMember(x => x.Expertise, opt => opt.Ignore());
            CreateMap<PreferredExpertise, PreferredExpertise>().ForMember(x => x.Expertise, opt => opt.Ignore());
            CreateMap<PreferredExpertise, PrefferedExpertiseDTO>();
            CreateMap<CurrencyInfo, CurrencyInfoDTO>();
        }
    }
}
