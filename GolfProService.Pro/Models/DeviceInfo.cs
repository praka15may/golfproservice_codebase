﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace GolfProService.Pro.Models
{
    public class DeviceInfo
    {
        [Key]
        public int DeviceInfoID { get; set; }
        public string UUID { get; set; }
        public string Device_Type { get; set; }
        public string Device_Platform { get; set; }
        public string UA_Token { get; set; }
        public string IOS_Token { get; set; }
        public string Android_Token { get; set; }
        public string Status { get; set; }
    }
}
