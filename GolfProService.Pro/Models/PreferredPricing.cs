﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace GolfProService.Pro.Models
{
    public class PreferredPricing
    {
        [Key]
        public int PreferredPricingID { get; set; }
        public double Individual { get; set; }
        public double Group { get; set; }
        public double Package { get; set; }
        public int CurrencyInfoID { get; set; }
        [ForeignKey("CurrencyInfoID")]
        public virtual  CurrencyInfo CurrencyInfo { get; set; }
        public int Duration { get; set; }
        public string Status { get; set; }
     
    }
}
