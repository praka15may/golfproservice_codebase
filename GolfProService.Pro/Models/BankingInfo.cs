﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace GolfProService.Pro.Models
{
    public class BankAccountInfo
    {
        [Key]
        public int BankingInfoID { get; set; }
        public string AccountHolderName { get; set; }
        public string AccountType { get; set; }
        public string AccountHolderFirstName { get; set; }
        public string AccountHolderLastName { get; set; }
        public long AccountNumber { get; set; }
        public long RoutingNumber { get; set; }
        public string Status { get; set; }
    }
}
