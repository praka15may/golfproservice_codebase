﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace GolfProService.Pro.Models
{
    public class PreferredAgeLevel
    {
        [Key]
        public int PreferredAgeLevelID { get; set; }
        public int AgeLevelId { get; set; }
        [ForeignKey("AgeLevelId")]
        public virtual AgeLevel AgeLevel { get; set; }
        public string Status { get; set; }
    }
}
