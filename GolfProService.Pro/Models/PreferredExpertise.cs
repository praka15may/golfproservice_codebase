﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace GolfProService.Pro.Models
{
    public class PreferredExpertise
    {
        [Key]
        public int PreferredExpertiseID { get; set; }
        public int ExpertiseID { get; set; }
        [ForeignKey("ExpertiseID")]
        public virtual Expertise Expertise { get; set; }
        public string Status { get; set; }
    }
}
