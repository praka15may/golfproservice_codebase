﻿using GolfProService.Pro.Infrastructure.Message;
using GolfProService.Pro.Infrastructure.Rules;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace GolfProService.Pro.Models.DTO.ProProfile
{
    public class AddressDTO
    {
        [Required]
        [JsonProperty("address_1")]
        [StringLength(20, MinimumLength = 2, ErrorMessage = ValidationErrorMessage.STRING_LENGTH_MAX_MIN)]
        public string Address1 { get; set; }
        [JsonProperty("address_2")]
        public string Address2 { get; set; }
        [Required]
        [StringLength(20, MinimumLength = 2, ErrorMessage = ValidationErrorMessage.STRING_LENGTH_MAX_MIN)]
        [JsonProperty("city")]
        public string City { get; set; }
        [Required]
        [JsonProperty("state")]
        public string State { get; set; }
        [Required]
        [JsonProperty("zip")]
        [RegularExpression(RegularExpression.FOR_NUMBERS, ErrorMessage = ValidationErrorMessage.ACCEPT_NUMBER)]
        [StringLength(5, MinimumLength = 5, ErrorMessage = ValidationErrorMessage.NUMBER_LENGTH_MAX)]
        public string Zipcode { get; set; }
        [JsonProperty("country")]
        public string Country { get; set; }
    }
}
