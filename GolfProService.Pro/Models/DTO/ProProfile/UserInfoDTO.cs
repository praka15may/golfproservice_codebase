﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GolfProService.Pro.Models.DTO.ProProfile
{
    public class UserInfoDTO
    {
        [JsonProperty("id")]
        public int UserID { get; set; }
        [JsonProperty("email")]
        public string Email { get; set; }
        [JsonProperty("first_name")]
        public string FirstName { get; set; }
        [JsonProperty("last_name")]
        public string LastName { get; set; }
        [JsonProperty("born_on")]
        public string DateOfBirth { get; set; }
        [JsonProperty("gender")]
        public string Gender { get; set; }
        [JsonProperty("address_1")]
        public string Address { get; set; }
        [JsonProperty("address_city")]
        public string City { get; set; }
        [JsonProperty("address_state")]
        public string State { get; set; }
        [JsonProperty("address_zipcode")]
        public string Zipcode { get; set; }
        [JsonProperty("address_country")]
        public string Country { get; set; }
        [JsonProperty("phone")]
        public string Phone { get; set; }
        [JsonProperty("avatar_url")]
        public string OldImageUrl { get; set; }
        [JsonProperty("new_avatar_url")]
        public string NewImageUrl { get; set; }
        [JsonProperty("avatar")]
        public string Base64Image { get; set; }
    }
}
