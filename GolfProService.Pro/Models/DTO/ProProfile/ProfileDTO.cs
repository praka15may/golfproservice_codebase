﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GolfProService.Pro.Models.DTO.ProProfile
{
    public class ProfileDTO
    {
        [JsonProperty("pro")]
        public ProInfoDTO ProfileInfo { get; set; }
    }
}
