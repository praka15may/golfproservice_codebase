﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace GolfProService.Pro.Models.DTO.ProProfile
{
    public class ProfilePersonalInfoDTO
    {
        [JsonProperty("id")]
        public int id { get; set; }
        [JsonProperty("personal")]
        public PersonalInfoDTO PersonalInfo { get; set; }
        [JsonProperty("address")]
        public AddressDTO Address { get; set; }
        [Required]
        [JsonProperty("dob")]
        public string DateOfBirth { get; set; }
        [JsonProperty("courseid")]
        public int CourseID { get; set; }
        [JsonProperty("pgacertified")]
        public bool PGACertified { get; set; }
        [JsonProperty("gender")]
        public string Gender { get; set; }
        [JsonProperty("avatar")]
        public string Base64Image { get; set; }
        [JsonProperty("prodescription")]
        public string ProDescription { get; set; }
    }
}
