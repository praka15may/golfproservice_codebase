﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GolfProService.Pro.Models.DTO.ProProfile
{
    public class AccoladeDTO
    {
        [JsonProperty("id")]
        public int AccoladeID { get; set; }
        [JsonProperty("title")]
        public string Name { get; set; }
        [JsonProperty("status")]
        public string Status { get; set; }
    }
}
