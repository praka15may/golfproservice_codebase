﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GolfProService.Pro.Models.DTO.ProProfile
{
    public class PreferredTeachingStyleDTO
    {
        [JsonProperty("expertiselevels")]
        public List<PrefferedExpertiseDTO> ExpertiseLevels { get; set; }
        [JsonProperty("agelevels")]
        public List<PreferredAgeLevelDTO> AgeLevels { get; set; }
        [JsonProperty("accolades")]
        public List<AccoladeDTO> Accolades { get; set; }
        [JsonProperty("notableplayerscoached")]
        public List<NotablePlayersCoachedDTO> NotablePlayersCoached { get; set; }
        [JsonProperty("pgacertified")]
        public bool PGACertified { get; set; }
    }
}
