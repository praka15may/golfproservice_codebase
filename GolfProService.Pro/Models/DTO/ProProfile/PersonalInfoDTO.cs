﻿using GolfProService.Pro.Infrastructure.Message;
using GolfProService.Pro.Infrastructure.Rules;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace GolfProService.Pro.Models.DTO.ProProfile
{
    public class PersonalInfoDTO
    {
        [Required]
        [RegularExpression(RegularExpression.FOR_ALPHABETS_SPACE_NUMBER, ErrorMessage = ValidationErrorMessage.ALPHABETS_SPACE_NUMBER)]
        [StringLength(15, MinimumLength = 2, ErrorMessage = ValidationErrorMessage.STRING_LENGTH_MAX_MIN)]
        [JsonProperty("firstname")]
        public string FirstName { get; set; }
        [Required]
        [RegularExpression(RegularExpression.FOR_ALPHABETS_SPACE_NUMBER, ErrorMessage = ValidationErrorMessage.ALPHABETS_SPACE_NUMBER)]
        [StringLength(15, MinimumLength = 2, ErrorMessage = ValidationErrorMessage.STRING_LENGTH_MAX_MIN)]
        [JsonProperty("lastname")]
        public string LastName { get; set; }
        [JsonProperty("email")]
        [Required]
        [RegularExpression(RegularExpression.FOR_INVALID_EMAIL_FORMAT, ErrorMessage = ValidationErrorMessage.INVALID_EMAIL_FORMAT)]
        public string Email { get; set; }
        [JsonProperty("phone")]
        [RegularExpression(RegularExpression.FOR_NUMBERS, ErrorMessage = ValidationErrorMessage.ACCEPT_NUMBER)]
        [StringLength(10, MinimumLength = 10, ErrorMessage = ValidationErrorMessage.NUMBER_LENGTH_MAX)]
        public string Phone { get; set; }
    }
}
