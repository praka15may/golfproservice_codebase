﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GolfProService.Pro.Models.DTO.ProProfile
{
    public class BankInfoDTO
    {
        [JsonProperty("accounttypeId")]
        public int BankingInfoID { get; set; }
        [JsonProperty("accholdername")]
        public string AccountHolderName { get; set; }
        [JsonProperty("accounttype")]
        public string AccountType { get; set; }
        [JsonProperty("accholderfirstname")]
        public string AccountHolderFirstName { get; set; }
        [JsonProperty("accholderlastname")]
        public string AccountHolderLastName { get; set; }
        [JsonProperty("accountnumber")]
        public long AccountNumber { get; set; }
        [JsonProperty("routingnumber")]
        public long RoutingNumber { get; set; }
        [JsonProperty("status")]
        public string Status { get; set; }
    }
}
