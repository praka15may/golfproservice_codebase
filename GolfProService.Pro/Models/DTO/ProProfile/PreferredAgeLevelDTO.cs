﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GolfProService.Pro.Models.DTO.ProProfile
{
    public class PreferredAgeLevelDTO
    {
        [JsonProperty("id")]
        public int PreferredAgeLevelID { get; set; }
        [JsonProperty("agelevelid")]
        public int AgeLevelId { get; set; }
        [JsonProperty("status")]
        public string Status { get; set; }
        [JsonProperty("agelevel")]
        public AgeLevelDTO AgeLevel { get; set; }
    }
}
