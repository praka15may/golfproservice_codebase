﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GolfProService.Pro.Models.DTO.ProProfile
{
    public class ProQuickProfilBasicInfoDTO
    {
        [JsonProperty("id")]
        public int UserID { get; set; }
        [JsonProperty("name")]
        public string Name { get; set; }
        [JsonProperty("image_url")]
        public string ImageURL { get; set; }
        [JsonProperty("city")]
        public string City { get; set; }
        [JsonProperty("state")]
        public string State { get; set; }
        [JsonProperty("country")]
        public string Country { get; set; }
        [JsonProperty("zipcode")]
        public string Zipcode { get; set; }
        [JsonProperty("latitude")]
        public double? Latitude { get; set; }
        [JsonProperty("longitude")]
        public double? Longitude { get; set; }
        [JsonProperty("expertiselevel")]
        public List<string> ExpertiseLevel { get; set; }
        [JsonProperty("agerange")]
        public List<string> AgeRange { get; set; }
        [JsonProperty("price")]
        public PriceDTO Price { get; set; }
        [JsonProperty("courseid")]
        public int CourseID { get; set; }
    }
}
