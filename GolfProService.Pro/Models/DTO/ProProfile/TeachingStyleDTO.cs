﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GolfProService.Pro.Models.DTO.ProProfile
{
    public class TeachingStyleDTO
    {
        [JsonProperty("expertiselevels")]
        public List<PrefferedExpertiseDTO> ExpertiseLevels { get; set; }
        [JsonProperty("agelevels")]
        public List<PreferredAgeLevelDTO> AgeLevels { get; set; }
    }
}
