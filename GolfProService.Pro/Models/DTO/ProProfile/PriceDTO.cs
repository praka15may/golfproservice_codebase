﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GolfProService.Pro.Models.DTO.ProProfile
{
    public class PriceDTO
    {
        [JsonProperty("individual")]
        public double Individual { get; set; }
        [JsonProperty("group")]
        public double Group { get; set; }
        [JsonProperty("package")]
        public double Package { get; set; }
    }
}
