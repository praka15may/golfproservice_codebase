﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GolfProService.Pro.Models.DTO.ProProfile
{
    public class PrefferedExpertiseDTO
    {
        [JsonProperty("id")]
        public int PreferredExpertiseID { get; set; }
        [JsonProperty("expertiseid")]
        public int ExpertiseID { get; set; }
        [JsonProperty("status")]
        public string Status { get; set; }
        [JsonProperty("expertise")]
        public ExpertiseDTO Expertise { get; set; }
    }
}
