﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GolfProService.Pro.Models.DTO.ProProfile
{
    public class PreferredPricingDTO
    {
        [JsonProperty("id")]
        public int PreferredPricingID { get; set; }
        [JsonProperty("individual")]
        public double Individual { get; set; }
        [JsonProperty("group")]
        public double Group { get; set; }
        [JsonProperty("package")]
        public double Package { get; set; }
        [JsonProperty("currencyinfoid")]
        public int CurrencyInfoID { get; set; }
        [JsonProperty("currencyinfo")]
        public CurrencyInfoDTO CurrencyInfo { get; set; }
        [JsonProperty("status")]
        public string Status { get; set; }
    }
}
