﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GolfProService.Pro.Models.DTO.ProProfile
{
    public class ProQuickProfileDTO
    {
        [JsonProperty("proname")]
        public string ProName { get; set; }
        [JsonProperty("experience")]
        public string Experience { get; set; }
        [JsonProperty("agerange")]
        public string AgeRange { get; set; }
        [JsonProperty("individualrate")]
        public double IndividualRate { get; set; }
        [JsonProperty("grouprate")]
        public double GroupRate { get; set; }
        [JsonProperty("packagerate")]
        public double PackageRate { get; set; }
        [JsonProperty("award")]
        public string Award { get; set; }
        [JsonProperty("notableplayerscoached")]
        public string NotableplayesCoached { get; set; }
        [JsonProperty("pgacertified")]
        public bool PGACertified { get; set; }
        [JsonProperty("courseid")]
        public int CourseId { get; set; }
        [JsonProperty("prodescription")]
        public string ProDescription { get; set; }
    }
}
