﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GolfProService.Pro.Models.SystemEntities
{
    public interface IAuditableEntity
    {
        DateTime? UpdatedDate { get; set; }
        DateTime? CreatedDate { get; set; }
        string UpdatedByID { get; set; }
        string CreatedByID { get; set; }
    }
}
