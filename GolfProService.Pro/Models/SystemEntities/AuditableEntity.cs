﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace GolfProService.Pro.Models.SystemEntities
{
    public class AuditableEntity : IAuditableEntity
    {
        public DateTime? UpdatedDate { get; set; }
        public DateTime? CreatedDate { get; set; }
        public string UpdatedByID { get; set; }
        public string CreatedByID { get; set; }
    }
}
