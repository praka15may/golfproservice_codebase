﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace GolfProService.Pro.Models
{
    public class CurrencyInfo
    {
        [Key]
        public int CurrencyInfoID { get; set; }
        public string Country { get; set; }
        public string Currency { get; set; }
        public string Status { get; set; }
    }
}
