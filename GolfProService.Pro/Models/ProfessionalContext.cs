﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;

namespace GolfProService.Pro.Models
{
    public class ProfessionalContext : DbContext
    {
        public ProfessionalContext(DbContextOptions<ProfessionalContext> options) : base(options) { }
        public virtual DbSet<ProProfile> ProProfiles { get; set; }
        public virtual DbSet<PreferredPricing> PreferredPricing { get; set; }
        public virtual DbSet<Expertise> Expertises { get; set; }
        public virtual DbSet<BankAccountInfo> BankAccountInfos { get; set; }
        public virtual DbSet<AgeLevel> AgeLevels { get; set; }
        public virtual DbSet<DeviceInfo> DeviceInfos { get; set; }
        public virtual DbSet<NotablePlayersCoached> NotablePlayersCoached { get; set; }
        public virtual DbSet<PreferredAgeLevel> PreferredAgeLevels { get; set; }
        public virtual DbSet<PreferredExpertise> PreferredExpertises { get; set; }

    }
}
