﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using GolfProService.Pro.Infrastructure.Exceptions;
using GolfProService.Pro.Models.DTO.ProProfile;
using GolfProService.Pro.Services.Utility;

namespace GolfProService.Pro.Services.Professional
{
    internal class ProService : IProService
    {
        IServiceUtility ServiceUtility = null;
        public ProService(IServiceUtility serviceUtility)
        {
            ServiceUtility = serviceUtility;
        }

        public async Task<List<ProQuickProfilBasicInfoDTO>> GetProBasicInfoBySearch(string search)
        {
            List<ProQuickProfilBasicInfoDTO> quickProfiles = null;
            try
            {
                quickProfiles = await ServiceUtility.GetDataFromService<List<ProQuickProfilBasicInfoDTO>>("beta/pro/search/"+ search, "ProSearchAPI", "");
            }
            catch(Exception ex)
            {
                throw ex;
            }
            return quickProfiles;
        }

        /// <summary>
        /// Make call to marketplace api service to get User info
        /// </summary>
        /// <param name="userToken"></param>
        /// <returns></returns>
        public async Task<UserDTO> GetUserInfoByUserToken(string userToken)
        {
            UserDTO userInfo = null;
            try
            {
                userInfo = await ServiceUtility.GetDataFromService<UserDTO>("/api/v4/users?details=n", "MarketplaceStagingAPIs", userToken);
            }
            catch(UserNotAuthorizedException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return userInfo;
        }

        /// <summary>
        /// Marketplace api call to update the Pro profile info
        /// </summary>
        /// <param name="userToken"></param>
        /// <returns></returns>
        public async Task<bool> UpdateProfileInfoByToken(UserInfoDTO userProfileInfo, string userToken)
        {
            bool IsProfileUpdated = false;
            try
            {
                var response = await ServiceUtility.PutDataToService<object>("/api/v4/users", "MarketplaceStagingAPIs", userToken, userProfileInfo);
                IsProfileUpdated = true;
            }
            catch(Exception ex)
            {
                throw ex;
            }
            return IsProfileUpdated;
        }
    }
}
