﻿using GolfProService.Pro.Models.DTO.ProProfile;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GolfProService.Pro.Services.Professional
{
    public interface IProService
    {
        Task<UserDTO> GetUserInfoByUserToken(string userToken);
        Task<bool> UpdateProfileInfoByToken(UserInfoDTO userProfileInfo, string userToken);
        Task<List<ProQuickProfilBasicInfoDTO>> GetProBasicInfoBySearch(string search);
    }
}
