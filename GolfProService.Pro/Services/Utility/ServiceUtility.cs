﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Http.Headers;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Http.Extensions;
using System;
using System.Web;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using Microsoft.IdentityModel.Protocols;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using GolfProService.Pro.Infrastructure.Exceptions;

namespace GolfProService.Pro.Services.Utility
{
    public class ServiceUtility : IServiceUtility
    {
        public IConfiguration Configuration { get; }
        public static HttpContext currentContext { get; set; }

        public ServiceUtility(IConfiguration configuration)
        {
            Configuration = configuration;
        }       

        public async Task<TObject> GetDataFromService<TObject>(string URL, string serviceName, string userToken) where TObject : new()
        {
            try
            {
                TObject result = default(TObject);
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(Configuration[serviceName]);
                    client.DefaultRequestHeaders.Accept.Clear();
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                    client.DefaultRequestHeaders.Add("x-api-key", Configuration["MarketplaceStagingAPIKey"]);
                    if (userToken != "")
                        client.DefaultRequestHeaders.Add("x-api-user-token", userToken);
                    HttpResponseMessage response = await client.GetAsync(URL);
                    if (response.IsSuccessStatusCode)
                    {
                        string resultString = await response.Content.ReadAsStringAsync();
                       
                        result = JsonConvert.DeserializeObject<TObject>(resultString);
                    }    
                    else if(response.StatusCode == System.Net.HttpStatusCode.Unauthorized)
                    {
                        throw new UserNotAuthorizedException();
                    }
                    else
                    {
                        var ex = response.Content.ReadAsStringAsync().Result;
                        throw new Exception(ex);
                    }

                    return result;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public async Task<TObject> PostDataToService<TObject>(string URL, string serviceName, string userToken, object data) where TObject : new()
        {
            try
            {
                TObject result = default(TObject);
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(Configuration[serviceName]);
                    client.DefaultRequestHeaders.Accept.Clear();
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                    client.DefaultRequestHeaders.Add("x-api-key", Configuration["MarketplaceStagingAPIKey"]);
                    if(userToken != "")
                        client.DefaultRequestHeaders.Add("x-api-user-token", userToken);
                    var response = await client.PostAsync(URL, new StringContent(JsonConvert.SerializeObject(data), System.Text.Encoding.UTF8, "application/json"));
                    if (response.IsSuccessStatusCode)
                    {
                        string resultString = await response.Content.ReadAsStringAsync();
                        result = JsonConvert.DeserializeObject<TObject>(resultString);

                    }
                    else if (response.StatusCode == System.Net.HttpStatusCode.Unauthorized)
                    {
                        throw new UserNotAuthorizedException();
                    }
                    else
                    {
                        var ex = response.Content.ReadAsStringAsync().Result;
                        throw new Exception(ex);
                    }
                    return result;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public async Task<TObject> PutDataToService<TObject>(string URL, string serviceName, string userToken, object data) where TObject : new()
        {
            try
            {
                TObject result = default(TObject);
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(Configuration[serviceName]);
                    client.DefaultRequestHeaders.Accept.Clear();
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                    client.DefaultRequestHeaders.Add("x-api-key", Configuration["MarketplaceStagingAPIKey"]);
                    if (userToken != "")
                        client.DefaultRequestHeaders.Add("x-api-user-token", userToken);
                    var response = await client.PutAsync(URL, new StringContent(JsonConvert.SerializeObject(data), System.Text.Encoding.UTF8, "application/json"));
                    if (response.IsSuccessStatusCode)
                    {
                        string resultString = await response.Content.ReadAsStringAsync();
                        result = JsonConvert.DeserializeObject<TObject>(resultString);

                    }
                    else if (response.StatusCode == System.Net.HttpStatusCode.Unauthorized)
                    {
                        throw new UserNotAuthorizedException();
                    }
                    else
                    {
                        var ex = response.Content.ReadAsStringAsync().Result;
                        throw new Exception(ex);
                    }
                    return result;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
