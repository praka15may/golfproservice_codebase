﻿using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using GolfProService.Pro.Infrastructure.Exceptions;
using GolfProService.Pro.Infrastructure.Message;
using GolfProService.Pro.Models.DTO.ProProfile;
using GolfProService.Pro.Repository.Professional;
using GolfProService.Pro.Services.Professional;
using GolfProService.Pro.Services.Utility;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace GolfProService.Pro.Controllers
{
    [Route("api/Pro/[Action]")]
    public class ProController : Controller
    {
        string userToken = "";
        IProService ProService = null;
        IProRepository ProRepository = null;
        public static HttpContext currentContext { get; set; }

        public ProController(IProService proService, IProRepository proRepository)
        {
            ProService = proService;
            ProRepository = proRepository;            
            //userToken = "4834-$2a$10$RFoVArOWq5TcqoJI4zKOEu";
        }

        /// <summary>
        /// Get the Professional profile information
        /// </summary>
        /// <param name="userToken"></param>
        /// <returns></returns>
        [HttpGet]
        [ProducesResponseType(200, Type = typeof(ProfileDTO))]        
        public async Task<IActionResult> GetProProfileInfo()
        {
            HttpResponseMessage msg = null;
            UserDTO user = null;
            dynamic obj = new ExpandoObject();
            userToken = HttpContext.Request.Headers["X-Api-User-Token"].FirstOrDefault();
            try
            {
                if (userToken != null)
                {
                    user = await ProService.GetUserInfoByUserToken(userToken);
                    obj.Result = ProRepository.GetProfileInfoByUserID(user.UserInfo);
                }
                else
                {
                    throw new UnauthorizedAccessException();
                }                
              
            }            
            catch(UserNotAuthorizedException)
            {
                //obj.title = ExceptionMessage.NotAuthorized_Title;
                //obj.error = ExceptionMessage.NotAuthorized_Error;
                return Unauthorized();
            }
            catch(Exception ex)
            {
                throw ex;
            }

            return Ok(obj.Result);
        }

        /// <summary>
        /// Get all Expertise master information
        /// </summary>
        /// <param name="userToken"></param>
        /// <returns></returns>
        [HttpGet]
        [ProducesResponseType(200, Type = typeof(ExpertiseDTO))]
        public async Task<IActionResult> GetAllExpertise()
        {
            dynamic obj = new ExpandoObject();
            try
            {
                obj.Result = ProRepository.GetAllExpertise();

            }
            catch (Exception ex)
            {
                throw ex;
            }

            return Ok(obj.Result);
        }

        /// <summary>
        /// Get all Age level master information
        /// </summary>
        /// <param name="userToken"></param>
        /// <returns></returns>
        [HttpGet]
        [ProducesResponseType(200, Type = typeof(AgeLevelDTO))]
        public async Task<IActionResult> GetAllAgeLevels()
        {
            dynamic obj = new ExpandoObject();
            try
            {
                obj.Result = ProRepository.GetAllAgeLevels();
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return Ok(obj.Result);
        }

        /// <summary>
        /// Update professional personal profile information
        /// </summary>
        /// <param name="proInfo"></param>
        /// <returns></returns>
        [HttpPut]
        [ProducesResponseType(200, Type = typeof(ProfileDTO))]
        public async Task<IActionResult> UpdatePersonalInfo([FromBody]ProfilePersonalInfoDTO proInfo)
        {
            dynamic obj = new ExpandoObject();
            try
            {
                userToken = HttpContext.Request.Headers["X-Api-User-Token"].FirstOrDefault();
                if (userToken != null)
                {
                    if (ModelState.IsValid)
                    {
                        obj.Result = await ProRepository.UpdateProfileInfoByUserID(proInfo, userToken);
                    }
                    else
                    {
                        // Returns validation errors by comma seprated
                        obj.Title = ExceptionMessage.DataValidation_Title;
                        obj.error = String.Join(", ", ModelState.Keys.SelectMany(key => this.ModelState[key].Errors.Select(x => key + ": " + x.ErrorMessage)));
                        return BadRequest(obj);
                    }
                }                    
                else
                {
                    throw new UserNotAuthorizedException();
                }
                    
            }
            catch (UserNotAuthorizedException)
            {
                //obj.title = ExceptionMessage.NotAuthorized_Title;
                //obj.error = ExceptionMessage.NotAuthorized_Error;
                return Unauthorized();
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }

            return Ok(obj.Result);
        }

        /// <summary>
        /// Update Pro Bank Account Information
        /// </summary>
        /// <param name="userID"></param>
        /// <param name="bankInfo"></param>
        /// <returns></returns>
        [HttpPut]
        [ProducesResponseType(200, Type = typeof(BankInfoDTO))]
        public IActionResult UpdateBankInfo(int userID, [FromBody]BankInfoDTO bankInfo)
        {
            dynamic obj = new ExpandoObject();
            try
            {
                obj.Result = ProRepository.AddOrUpdateBankInfo(userID, bankInfo);
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return Ok(obj.Result);
        }

        /// <summary>
        /// Update Pro Preferred pricing information
        /// </summary>
        /// <param name="userID"></param>
        /// <param name="pricingRate"></param>
        /// <returns></returns>
        [HttpPut]
        [ProducesResponseType(200, Type = typeof(PreferredPricingDTO))]
        public IActionResult UpdatePricingInfo(int userID, [FromBody]PreferredPricingDTO pricingRate)
        {
            dynamic obj = new ExpandoObject();
            try
            {
                obj.Result = ProRepository.AddOrUpdatPreferredPricing(userID, pricingRate);
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return Ok(obj.Result);
        }

        /// <summary>
        /// Update Pro teaching style(Expertise level, Age levle, Notable players coached, Accolades and PGA certified)
        /// </summary>
        /// <param name="userID"></param>
        /// <param name="preferredTeachingStyleDTO"></param>
        /// <returns></returns>
        [HttpPut]
        [ProducesResponseType(200, Type = typeof(PreferredTeachingStyleDTO))]
        public IActionResult UpdatePreferredTeachingStyle(int userID, [FromBody]PreferredTeachingStyleDTO preferredTeachingStyleDTO)
        {
            dynamic obj = new ExpandoObject();
            try
            {
                obj.Result = ProRepository.AddOrUpdateTeachingStyle(userID, preferredTeachingStyleDTO);
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return Ok(obj.Result);
        }

        [HttpGet]
        [ProducesResponseType(200, Type = typeof(ProQuickProfileDTO))]
        public IActionResult GetProQuickProfileByUserID(int userID)
        {
            dynamic obj = new ExpandoObject();
            try
            {
                obj.Result = ProRepository.GetProQuickProfileInfo(userID);
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return Ok(obj.Result);
        }

        [HttpGet] 
        [ProducesResponseType(200, Type = typeof(ProQuickProfilBasicInfoDTO))]
        public async Task<IActionResult> GetProBasicInfoBySearch(string search)
        {
            dynamic obj = new ExpandoObject();
            try
            {
                var proProfiles = await ProService.GetProBasicInfoBySearch(search);
                if (proProfiles.Count > 0)
                    obj.Result = ProRepository.GetProQuickProfileBasicInfo(proProfiles);
                else
                    throw new NoProFoundException();
            }
            catch (NoProFoundException)
            {
                obj.Title = ExceptionMessage.NoProFound_Title;
                obj.Error = ExceptionMessage.NoProFound_Error;
                return BadRequest(obj);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }

            return Ok(obj.Result);
        }

    }
}