﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using GolfProService.Pro.Infrastructure.Filters;
using GolfProService.Pro.Models;
using GolfProService.Pro.Repository.Professional;
using GolfProService.Pro.Services.Professional;
using GolfProService.Pro.Services.Utility;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Cors.Internal;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Microsoft.Extensions.PlatformAbstractions;
using Swashbuckle.AspNetCore.Swagger;

namespace GolfProService.Pro
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddMvc();
            services.AddAutoMapper();

            // Register application services.
            services.AddScoped<IServiceUtility, ServiceUtility>();
            services.AddScoped<IProService, ProService>();
            services.AddScoped<IProRepository, ProRepository>();

            //var basePath = PlatformServices.Default.Application.ApplicationBasePath;
            //var xmlPath = Path.Combine(basePath, "GolfProService.Pro.xml");
            services.AddCors(options =>
            {
                options.AddPolicy("AllowAny",
                   builder =>
                               builder.AllowAnyOrigin()
                                      .AllowAnyMethod()
                                      .AllowAnyHeader());
            });
            services.Configure<MvcOptions>(options =>
            {
                options.Filters.Add(new CorsAuthorizationFilterFactory("AllowAny"));
            });
            // Register the Swagger generator, defining one or more Swagger documents
            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new Info { Title = "GolfPro API", Version = "v1" });
                //c.IncludeXmlComments(xmlPath);
                c.OperationFilter<AddUserTokenHeaderParameter>();
            });
            services.AddDbContext<ProfessionalContext>(options => options.UseSqlServer(Configuration.GetConnectionString("ProfessionalDatabase")));

        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseSwagger();

            // Enable middleware to serve swagger-ui (HTML, JS, CSS, etc.), specifying the Swagger JSON endpoint.
            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("/swagger/v1/swagger.json", "GolfPro API v1");
            });
            app.UseCors("AllowAny");
            app.UseMvc();
        }
    }
}
