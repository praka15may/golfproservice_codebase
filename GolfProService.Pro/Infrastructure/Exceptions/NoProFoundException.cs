﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GolfProService.Pro.Infrastructure.Exceptions
{
    public class NoProFoundException : ApplicationException
    {
        public NoProFoundException()
        {

        }

        public NoProFoundException(string message) : base(message)
        {

        }

        public NoProFoundException(string message, Exception innerException) : base(message, innerException)
        {

        }
    }
}
