﻿using System;
using System.Net.Http;
using System.Runtime.Serialization;

namespace GolfProService.Pro.Infrastructure.Exceptions
{
    internal class UserNotAuthorizedException : ApplicationException
    {
        public UserNotAuthorizedException()
        {
        }
        public UserNotAuthorizedException(string message) : base(message)
        {
        }

        public UserNotAuthorizedException(string message, Exception innerException) : base(message, innerException)
        {
        }
        
    }
}