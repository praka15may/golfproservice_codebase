﻿using Microsoft.AspNetCore.Mvc.ApiExplorer;
using Swashbuckle.AspNetCore.Swagger;
using Swashbuckle.AspNetCore.SwaggerGen;
using GolfProService.Pro.Infrastructure.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection.Metadata;
using System.Threading.Tasks;

namespace GolfProService.Pro.Infrastructure.Filters
{
    public class AddUserTokenHeaderParameter : IOperationFilter
    {
        public void Apply(Operation operation, OperationFilterContext context)
        {
            if (operation.Parameters == null)
                operation.Parameters = new List<IParameter>();

            operation.Parameters.Add(new Models.Parameter
            {
                Name = "X-Api-User-Token",
                @In = "header",
                Type = "string",
                Required = false
            });
        }
    }

}
