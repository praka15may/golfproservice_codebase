﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GolfProService.Pro.Infrastructure.Rules
{
    public static class RegularExpression
    {
        public const string FOR_ALPHABETS_SPACE_NUMBER = @"^[a-zA-Z0-9 ]*$";
        public const string FOR_INVALID_EMAIL_FORMAT = @"^[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,5}$";
        public const string FOR_NUMBERS = @"([0-9]+)";
    }
}
