﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GolfProService.Pro.Infrastructure.Message
{
    public static class ExceptionMessage
    {
        public static string NotAuthorized_Title = "Not Authorized User";
        public static string NotAuthorized_Error = "Please provide valid user token";
        public static string DataValidation_Title = "Data Validation Error";
        public static string NoProFound_Title = "No Pro's Found";
        public static string NoProFound_Error = "You can try broadening your search by adjusting your filters above or you can reset all filters";
    }
}
